"use strict";

/**
 * Webpack object.
 *
 * @type {webpack | ((options: webpack.Configuration[]) => webpack.MultiCompiler) | ((options: (webpack.Configuration[] | webpack.MultiConfigurationFactory), handler: webpack.MultiCompiler.Handler) => (webpack.MultiWatching | webpack.MultiCompiler)) | ((options?: webpack.Configuration) => webpack.Compiler) | ((options: (webpack.Configuration | webpack.ConfigurationFactory), handler: webpack.Compiler.Handler) => (webpack.Compiler.Watching | webpack.Compiler))}
 */
const webpack = require("webpack");

/**
 * Path resolving utility.
 *
 * @type {path.PlatformPath | path}
 */
const path = require("path");

/**
 * Utility for searching files by pattern.
 *
 * @type {G | ((pattern: string, options: G.IOptions, cb: (err: (Error | null), matches: string[]) => void) => G.IGlob) | ((pattern: string, cb: (err: (Error | null), matches: string[]) => void) => G.IGlob)}
 */
const glob = require("glob");

/**
 * Merge function for webpack configuration objects.
 *
 * @type {merge}
 */
const merge = require("webpack-merge");

/**
 * File system utilities.
 *
 * @type {{mkdirpSync: function(*=, *=): (*), ensureFileSync: function(*=): (undefined), createSymlinkSync: function(*=, *=, *=): (any), removeSync: *, emptydirSync: function(*=): (void|undefined), moveSync: function(*=, *=, *=): undefined, pathExists: *, ensureDirSync: function(*=, *=): (*), createFile: *, createLink: *, remove: *, ensureLinkSync: function(*=, *=): (any), writeJson: *, readJsonSync: *, outputFile: *, ensureSymlink: *, emptyDir: *, mkdirsSync: function(*=, *=): (*), writeJsonSync: *, copy: *, readJson: *, ensureFile: *, ensureSymlinkSync: function(*=, *=, *=): (any), move: *, ensureLink: *, createSymlink: *, ensureDir: *, copySync: function(*=, *=, *=): undefined|void, emptyDirSync: function(*=): (void|undefined), mkdirp: *, createFileSync: function(*=): (undefined), emptydir: *, mkdirs: *, outputFileSync: function(*=, ...[*]): (*|undefined), pathExistsSync: *, createLinkSync: function(*=, *=): (any)}}
 */
const fs = require("fs-extra");

// Plugins
/**
 * Extracts CSS from JS files.
 *
 * @type {MiniCssExtractPlugin}
 */
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

/**
 * Cleans the workspace before or after the build.
 */
const {CleanWebpackPlugin} = require("clean-webpack-plugin");

/**
 * Copies assets by pattern.
 *
 * @type {CopyPlugin}
 */
const CopyPlugin = require("copy-webpack-plugin");

/**
 * Image minification plugin.
 *
 * @type {ImageminWebpackPlugin}
 */
const ImageminPlugin = require("imagemin-webpack-plugin").default;

/**
 * Path for emitting assets.
 *
 * Temporary path is used to ensure that no errors occured before
 * replacing actual production assets.
 *
 * @type {string}
 */
const tempBuildPath = "./sources/tmp";

/**
 * Path to production assets.
 *
 * @type {string}
 */
const assetsPath = "./assets";

/**
 * A list of static entries.
 *
 * @type {{generalblog: string[]}}
 */
const entries = {
  generalblog: glob.sync("./sources/js/parts/*.js"),
};

/**
 * Creates a data structure that can be consumed by webpack as an entry point.
 *
 * @param files
 *   Array of file paths to create entries from.
 *
 * @returns {{}}
 *   An object with formatted entries.
 */
function formatEntries(files) {
  if (files.length !== 0) {
    let sourcesObject = {};
    files.forEach(function (filename) {
      let propertyName = path.parse(filename.replace(".es6", "_es6")).name;
      sourcesObject[`${propertyName}`] = `${filename}`;
    });

    return sourcesObject;
  }
}

/**
 * Provides an array of entries.
 *
 * @param jsSources
 *   Pattern for javascript files to create entries from.
 * @param scssSources
 *   Pattern for SCSS files to create entries from.
 *
 * @returns {(function(...[*]=))|{bootstrapper: string[]}}
 *   An object with correctly formatted entries.
 */
function provideEntries(jsSources, scssSources) {
  if (jsSources.length !== 0 && scssSources.length !== 0) {
    let jsFiles = glob.sync(jsSources);
    let scssFiles = glob.sync(scssSources);

    if (jsFiles.length !== 0 || scssFiles.length !== 0) {
      let sourceFiles = merge(formatEntries(jsFiles), formatEntries(scssFiles));

      return merge(entries, sourceFiles);
    } else {
      return entries;
    }
  }
}

module.exports = {
  entry: () => provideEntries("./sources/js/*.js", "./sources/sass/generalblog/*.scss"),

  output: {
    filename: "js/[name].js",
    path: path.resolve(__dirname, "../sources/tmp")
  },

  externals: {
    // require("jquery") is external and available
    //  on the global var jQuery.
    "jquery": "jQuery"
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
            }
          },
          {
            loader: "postcss-loader",
            options: {
              parser: "postcss-safe-parser",
              sourceMap: true,
              ident: "postcss",
              plugins: [
                require("doiuse")({
                  browsers: [
                    "> 1%",
                    "last 2 versions",
                    "not ie <= 11",
                    "not ie_mob <= 11",
                    "not op_mob <= 46",
                    "not op_mini all",
                    "not Baidu < 8",
                    "not bb < 11",
                    "not android < 56"
                  ],
                  ignore: [
                    "viewport-units",
                    "text-size-adjust",
                    "css-appearance",
                    "css-filters",
                    "css-masks",
                    "css-featurequeries",
                    "css-sticky",
                    "css-clip-path",
                    "css-resize",
                    "css-touch-action",
                    "transforms3d",
                    "flexbox",
                    "will-change",
                    "multicolumn",
                    "pointer"
                  ],
                  ignoreFiles: []
                }),
                require("postcss-will-change"),
                require("postcss-sorting")({
                  "sort-order": "default"
                }),
                require("postcss-sort-media-queries")({
                  sort: "mobile-first"
                }),
                require("autoprefixer"),
                require("postcss-pxtorem")({
                  rootValue: 16,
                  unitPrecision: 5,
                  replace: true,
                  propWhiteList: [],
                  selectorBlackList: [/^html$/],
                  mediaQuery: false,
                  minPixelValue: 0
                }),
                require("postcss-reporter"),
              ]
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
              sassOptions: {
                precision: 8,
                includePaths: [
                  // Include self for access to files via package name.
                  "sources/sass/generalblog"
                ],
              }
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|jpeg|gif|ico|svg)$/,
        use: [{
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "images",
          }
        }]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "fonts",
            },
          }
        ]
      },
    ]
  },
  plugins: [
    new webpack.ProgressPlugin({profile: false}),
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: ["**\/*\.*", "!\.gitkeep", "!**\/\.gitkeep"],
      verbose: true,
    }),
    new MiniCssExtractPlugin({
      filename: "css/[name].css",
    }),
    new CopyPlugin({
      patterns: [
        {
          from: "sources/fonts",
          to: path.resolve(__dirname, "../sources/tmp/fonts/"),
          noErrorOnMissing: true
        },
        {
          from: "sources/images",
          to: path.resolve(__dirname, "../sources/tmp/images/"),
          noErrorOnMissing: true
        },
      ]
    }),
    new ImageminPlugin({test: /\.(jpe?g|png|gif|svg)$/i}),
    // Check if build has done without compilation errors and synchronize assets from temporary directory.
    {
      apply: (compiler) => {
        compiler.hooks.done.tap("AssetsSync", stats => {
          if (stats.hasErrors() === false) {
            // Checking files existence in temporary directory
            fs.exists(tempBuildPath, function (exists) {
              if (exists) {
                // Clear assets directory
                fs.removeSync(assetsPath);
                // Copy new build from temporary directory to assets
                fs.copy(
                  tempBuildPath,
                  assetsPath,
                  function (err) {
                    if (err) {
                      console.error(err);
                    } else {
                      console.log("Assets successfully sync!");
                    }
                  }
                );
              }
            });
          }
        });
      },
    },
  ],
};
