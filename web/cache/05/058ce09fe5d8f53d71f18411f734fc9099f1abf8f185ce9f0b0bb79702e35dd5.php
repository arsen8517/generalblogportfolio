<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/content/paragraph/paragraph.html.twig */
class __TwigTemplate_805569bfc4a6ad50b48cbe5ec120ff581b0ded93f6178008911e5f5efe6e8dd5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'paragraph' => [$this, 'block_paragraph'],
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["block"] = _glisseo_bem_block([0 => twig_get_attribute($this->env, $this->source,         // line 3
($context["paragraph"] ?? null), "entityTypeId", [], "method", false, false, true, 3), 1 => (((twig_get_attribute($this->env, $this->source,         // line 4
($context["paragraph"] ?? null), "bundle", [], "method", false, false, true, 4) != twig_get_attribute($this->env, $this->source, ($context["paragraph"] ?? null), "entityTypeId", [], "method", false, false, true, 4))) ? (twig_get_attribute($this->env, $this->source, ($context["paragraph"] ?? null), "bundle", [], "method", false, false, true, 4)) : ("")), 2 => (((        // line 5
($context["view_mode"] ?? null) != "default")) ? (($context["view_mode"] ?? null)) : (""))]);
        // line 9
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 10
($context["block"] ?? null), 10, $this->source))];
        // line 13
        echo "<div";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 13), 13, $this->source), "html", null, true);
        echo ">
  ";
        // line 14
        $this->displayBlock('paragraph', $context, $blocks);
        // line 21
        echo "</div>
";
    }

    // line 14
    public function block_paragraph($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "    <div class=\"";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 15, $this->source), "content"), "html", null, true);
        echo "\">
      ";
        // line 16
        $this->displayBlock('content', $context, $blocks);
        // line 19
        echo "    </div>
  ";
    }

    // line 16
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null), 17, $this->source), "html", null, true);
        echo "
      ";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/content/paragraph/paragraph.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 17,  77 => 16,  72 => 19,  70 => 16,  65 => 15,  61 => 14,  56 => 21,  54 => 14,  49 => 13,  47 => 10,  46 => 9,  44 => 5,  43 => 4,  42 => 3,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{%
  set block = bem_block([
    paragraph.entityTypeId(),
    paragraph.bundle() != paragraph.entityTypeId() ? paragraph.bundle(),
    view_mode != 'default' ? view_mode,
  ])
%}
{%
  set classes = [
    bem(block),
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% block paragraph %}
    <div class=\"{{ bem(block, 'content') }}\">
      {% block content %}
        {{ content }}
      {% endblock %}
    </div>
  {% endblock paragraph %}
</div>
", "themes/contrib/glisseo/templates/content/paragraph/paragraph.html.twig", "/app/web/themes/contrib/glisseo/templates/content/paragraph/paragraph.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 2, "block" => 14);
        static $filters = array("escape" => 13);
        static $functions = array("bem_block" => 2, "bem" => 10);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block'],
                ['escape'],
                ['bem_block', 'bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
