<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/field/field--comment.html.twig */
class __TwigTemplate_b6a70387da34425dbcd632f39c135f4d3ca3cbf3917230f194728641dec68ab8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["field_block"] = "field";
        // line 3
        $context["field_modifiers"] = [0 => ("name-" . $this->sandbox->ensureToStringAllowed(        // line 4
($context["field_name"] ?? null), 4, $this->source)), 1 => ("type-" . $this->sandbox->ensureToStringAllowed(        // line 5
($context["field_type"] ?? null), 5, $this->source)), 2 => ("label-" . $this->sandbox->ensureToStringAllowed(        // line 6
($context["label_display"] ?? null), 6, $this->source))];
        // line 10
        $context["entity_block"] = _glisseo_bem_block([0 =>         // line 11
($context["entity_type"] ?? null), 1 => (((        // line 12
($context["bundle"] ?? null) != ($context["entity_type"] ?? null))) ? (($context["bundle"] ?? null)) : ("")), 2 =>         // line 13
($context["field_name"] ?? null)]);
        // line 17
        $context["entity_modifiers"] = [0 =>         // line 18
($context["view_mode"] ?? null)];
        // line 22
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 23
($context["entity_block"] ?? null), 23, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["entity_modifiers"] ?? null), 23, $this->source)), 1 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 24
($context["field_block"] ?? null), 24, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["field_modifiers"] ?? null), 24, $this->source))];
        // line 28
        $context["title_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 29
($context["entity_block"] ?? null), 29, $this->source), "title"), 1 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 30
($context["field_block"] ?? null), 30, $this->source), "title")];
        // line 34
        $context["content_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 35
($context["entity_block"] ?? null), 35, $this->source), "form-title"), 1 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 36
($context["field_block"] ?? null), 36, $this->source), "form-title")];
        // line 39
        echo "<a id=\"comments\"></a>
<section";
        // line 40
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 40), 40, $this->source), "html", null, true);
        echo ">
  ";
        // line 41
        if ((($context["comments"] ?? null) &&  !($context["label_hidden"] ?? null))) {
            // line 42
            echo "    ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null), 42, $this->source), "html", null, true);
            echo "
    <h2";
            // line 43
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["title_attributes"] ?? null), "addClass", [0 => ($context["title_classes"] ?? null)], "method", false, false, true, 43), 43, $this->source), "html", null, true);
            echo ">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t($this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 43, $this->source)));
            echo "</h2>
    ";
            // line 44
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null), 44, $this->source), "html", null, true);
            echo "
  ";
        }
        // line 46
        echo "
  <div class=\"";
        // line 47
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["entity_block"] ?? null), 47, $this->source), "content"), "html", null, true);
        echo " ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["field_block"] ?? null), 47, $this->source), "content"), "html", null, true);
        echo "\">
    ";
        // line 48
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["comments"] ?? null), 48, $this->source), "html", null, true);
        echo "
  </div>

  ";
        // line 51
        if (($context["comment_form"] ?? null)) {
            // line 52
            echo "    <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["entity_block"] ?? null), 52, $this->source), "form-container"), "html", null, true);
            echo " ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["field_block"] ?? null), 52, $this->source), "form-container"), "html", null, true);
            echo "\">
      <h2";
            // line 53
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method", false, false, true, 53), 53, $this->source), "html", null, true);
            echo ">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Add new comment"));
            echo "</h2>
      ";
            // line 54
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["comment_form"] ?? null), 54, $this->source), "html", null, true);
            echo "
    </div>
  ";
        }
        // line 57
        echo "</section>
";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/field/field--comment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 57,  121 => 54,  115 => 53,  108 => 52,  106 => 51,  100 => 48,  94 => 47,  91 => 46,  86 => 44,  80 => 43,  75 => 42,  73 => 41,  69 => 40,  66 => 39,  64 => 36,  63 => 35,  62 => 34,  60 => 30,  59 => 29,  58 => 28,  56 => 24,  55 => 23,  54 => 22,  52 => 18,  51 => 17,  49 => 13,  48 => 12,  47 => 11,  46 => 10,  44 => 6,  43 => 5,  42 => 4,  41 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set field_block = 'field' %}
{%
  set field_modifiers = [
    'name-' ~ field_name,
    'type-' ~ field_type,
    'label-' ~ label_display,
  ]
%}
{%
  set entity_block = bem_block([
    entity_type,
    bundle != entity_type ? bundle,
    field_name,
  ])
%}
{%
  set entity_modifiers = [
    view_mode,
  ]
%}
{%
  set classes = [
    bem(entity_block, null, entity_modifiers),
    bem(field_block, null, field_modifiers),
  ]
%}
{%
  set title_classes = [
    bem(entity_block, 'title'),
    bem(field_block, 'title'),
  ]
%}
{%
  set content_classes = [
    bem(entity_block, 'form-title'),
    bem(field_block, 'form-title'),
  ]
%}
<a id=\"comments\"></a>
<section{{ attributes.addClass(classes) }}>
  {% if comments and not label_hidden %}
    {{ title_prefix }}
    <h2{{ title_attributes.addClass(title_classes) }}>{{ label|t }}</h2>
    {{ title_suffix }}
  {% endif %}

  <div class=\"{{ bem(entity_block, 'content') }} {{ bem(field_block, 'content') }}\">
    {{ comments }}
  </div>

  {% if comment_form %}
    <div class=\"{{ bem(entity_block, 'form-container') }} {{ bem(field_block, 'form-container') }}\">
      <h2{{ content_attributes.addClass(content_classes) }}>{{ 'Add new comment'|t }}</h2>
      {{ comment_form }}
    </div>
  {% endif %}
</section>
", "themes/contrib/glisseo/templates/field/field--comment.html.twig", "/app/web/themes/contrib/glisseo/templates/field/field--comment.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 41);
        static $filters = array("escape" => 40, "t" => 43);
        static $functions = array("bem_block" => 10, "bem" => 23);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape', 't'],
                ['bem_block', 'bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
