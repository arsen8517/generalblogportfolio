<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/scholar_new/templates/modules/html.html.twig */

class __TwigTemplate_0d8dad8eeff6a7b4e224de6fe49de335287cb6883fb5f4b42b07dbd341ca2894 extends \Twig\Template
{
  private $source;
  private $macros = [];

  public function __construct(Environment $env)
  {
    parent::__construct($env);

    $this->source = $this->getSourceContext();

    $this->parent = false;

    $this->blocks = [
    ];
    $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
    $this->checkSecurity();
  }

  public function getTemplateName()
  {
    return "themes/custom/scholar_new/templates/modules/html.html.twig";
  }

  public function isTraitable()
  {
    return false;
  }

  public function getDebugInfo()
  {
    return array(90 => 48, 86 => 47, 82 => 46, 78 => 45, 73 => 43, 70 => 42, 66 => 37, 61 => 35, 57 => 34, 53 => 33, 49 => 32, 44 => 30, 39 => 28,);
  }

  public function getSourceContext()
  {
    return new Source("{#
/**
 * @file
 * Default theme implementation for the basic structure of a single Drupal page.
 *
 * Variables:
 * - logged_in: A flag indicating if user is logged in.
 * - root_path: The root path of the current page (e.g., node, admin, user).
 * - node_type: The content type for the current node, if the page is a node.
 * - head_title: List of text elements that make up the head_title variable.
 *   May contain one or more of the following:
 *   - title: The title of the page.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site.
 * - page_top: Initial rendered markup. This should be printed before 'page'.
 * - page: The rendered page markup.
 * - page_bottom: Closing rendered markup. This variable should be printed after
 *   'page'.
 * - db_offline: A flag indicating if the database is offline.
 * - placeholder_token: The token for generating head, css, js and js-bottom
 *   placeholders.
 *
 * @see template_preprocess_html()
 *
 * @ingroup themeable
 */
#}
{{ attach_library('scholar_new/landing-page') }}
<!DOCTYPE html>
<html{{ html_attributes }}>
<head>
  <head-placeholder token=\"{{ placeholder_token }}\">
    <title>{{ head_title|safe_join(' | ') }}</title>
    <css-placeholder token=\"{{ placeholder_token }}\">
      <js-placeholder token=\"{{ placeholder_token }}\">
        </head>
<body{{ attributes }}>
{#
Keyboard navigation/accessibility link to main content section in
secondBasePage.html.twig.
#}
<a href=\"#main-content\" class=\"visually-hidden focusable\">
  {{ 'Skip to main content'|t }}
</a>
{{ page_top }}
{{ page }}
{{ page_bottom }}
<js-bottom-placeholder token=\"{{ placeholder_token }}\">
</body>
</html>
", "themes/custom/scholar_new/templates/modules/html.html.twig", "/app/web/themes/custom/scholar_new/templates/modules/html.html.twig");
  }

  public function checkSecurity()
  {
    static $tags = array();
    static $filters = array("escape" => 28, "safe_join" => 33, "t" => 43);
    static $functions = array("attach_library" => 28);

    try {
      $this->sandbox->checkSecurity(
        [],
        ['escape', 'safe_join', 't'],
        ['attach_library']
      );
    } catch (SecurityError $e) {
      $e->setSourceContext($this->source);

      if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
        $e->setTemplateLine($tags[$e->getTagName()]);
      } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
        $e->setTemplateLine($filters[$e->getFilterName()]);
      } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
        $e->setTemplateLine($functions[$e->getFunctionName()]);
      }

      throw $e;
    }

  }

  protected function doDisplay(array $context, array $blocks = [])
  {
    $macros = $this->macros;
    // line 28
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->attachLibrary("scholar_new/landing-page"), "html", null, true);
    echo "
<!DOCTYPE html>
<html";
    // line 30
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["html_attributes"] ?? null), 30, $this->source), "html", null, true);
    echo ">
<head>
  <head-placeholder token=\"";
    // line 32
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 32, $this->source), "html", null, true);
    echo "\">
    <title>";
    // line 33
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->safeJoin($this->env, $this->sandbox->ensureToStringAllowed(($context["head_title"] ?? null), 33, $this->source), " | "));
    echo "</title>
    <css-placeholder token=\"";
    // line 34
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 34, $this->source), "html", null, true);
    echo "\">
      <js-placeholder token=\"";
    // line 35
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 35, $this->source), "html", null, true);
    echo "\">
        </head>
<body";
    // line 37
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null), 37, $this->source), "html", null, true);
    echo ">
";
    // line 42
    echo "<a href=\"#main-content\" class=\"visually-hidden focusable\">
  ";
    // line 43
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Skip to main content"));
    echo "
</a>
";
    // line 45
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_top"] ?? null), 45, $this->source), "html", null, true);
    echo "
";
    // line 46
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page"] ?? null), 46, $this->source), "html", null, true);
    echo "
";
    // line 47
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_bottom"] ?? null), 47, $this->source), "html", null, true);
    echo "
<js-bottom-placeholder token=\"";
    // line 48
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 48, $this->source), "html", null, true);
    echo "\">
</body>
</html>
";
  }
}
