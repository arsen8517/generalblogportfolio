<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/generalblog/templates/layout/secondBasePage.html.twig */

class __TwigTemplate_4f77cf0d92aadcf66b08d8555521d3f4a92afb535ae2d2f9d83a239e726c91e4 extends \Twig\Template
{
  private $source;
  private $macros = [];

  public function __construct(Environment $env)
  {
    parent::__construct($env);

    $this->source = $this->getSourceContext();

    $this->parent = false;

    $this->blocks = [
    ];
    $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
    $this->checkSecurity();
  }

  public function getTemplateName()
  {
    return "themes/custom/generalblog/templates/layout/secondBasePage.html.twig";
  }

  public function isTraitable()
  {
    return false;
  }

  public function getDebugInfo()
  {
    return array(128 => 84, 124 => 83, 119 => 82, 117 => 81, 114 => 80, 112 => 79, 111 => 78, 110 => 77, 109 => 76, 102 => 71, 96 => 68, 91 => 67, 89 => 66, 84 => 64, 79 => 63, 73 => 60, 68 => 59, 66 => 58, 63 => 57, 57 => 55, 55 => 54, 51 => 53, 42 => 47, 39 => 46,);
  }

  public function getSourceContext()
  {
    return new Source("{#
/**
 * @file
 * Theme override to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Page content (in order of occurrence in the default secondBasePage.html.twig):
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.primary_menu: Items for the primary menu region.
 * - page.secondary_menu: Items for the secondary menu region.
 * - page.highlighted: Items for the highlighted content region.
 * - page.help: Dynamic help text, mostly for admin pages.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.footer: Items for the footer region.
 * - page.top_footer: Items for the top_footer region.
 * - page.breadcrumb: Items for the breadcrumb region.
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 */
#}
<header class=\"my_header\">
  {{ page.header }}

</header>

<main role=\"main\">

  <div class=\"{{ b5_top_container }}\">
    {% if page.breadcrumb %}
      {{ page.breadcrumb }}
    {% endif %}
    <div class=\"row g-0\">
      {% if page.sidebar_first %}
        <div class=\"order-2 order-lg-1 {{ sidebar_first_classes }}\">
          {{ page.sidebar_first }}
        </div>
      {% endif %}
      <div class=\"order-1 order-lg-2 {{ content_classes }}\">
        {{ page.content }}
      </div>
      {% if page.sidebar_second %}
        <div class=\"order-3 {{ sidebar_second_classes }}\">
          {{ page.sidebar_second }}
        </div>
      {% endif %}
    </div>
  </div>

</main>

{% set footer_top_classes = ' ' ~
  (b5_footer_schema != 'none' ? \" footer-#{b5_footer_schema}\" : ' ') ~
  (b5_footer_schema != 'none' ? (b5_footer_schema == 'dark' ? ' text-light' : ' text-dark' ) : ' ') ~
  (b5_footer_bg_schema != 'none' ? \" bg-#{b5_footer_bg_schema}\" : ' ') %}

{% if page.footer_top %}
  <footer class=\"mt-auto {{ footer_classes }}\">
    <div class=\"{{ b5_top_container }}\">
      {{ page.footer }}
    </div>
  </footer>
{% endif %}
", "themes/custom/generalblog/templates/layout/secondBasePage.html.twig", "/app/web/themes/custom/generalblog/templates/layout/secondBasePage.html.twig");
  }

  public function checkSecurity()
  {
    static $tags = array("if" => 54, "set" => 76);
    static $filters = array("escape" => 47);
    static $functions = array();

    try {
      $this->sandbox->checkSecurity(
        ['if', 'set'],
        ['escape'],
        []
      );
    } catch (SecurityError $e) {
      $e->setSourceContext($this->source);

      if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
        $e->setTemplateLine($tags[$e->getTagName()]);
      } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
        $e->setTemplateLine($filters[$e->getFilterName()]);
      } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
        $e->setTemplateLine($functions[$e->getFunctionName()]);
      }

      throw $e;
    }

  }

  protected function doDisplay(array $context, array $blocks = [])
  {
    $macros = $this->macros;
    // line 46
    echo "<header class=\"my_header\">
  ";
    // line 47
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 47), 47, $this->source), "html", null, true);
    echo "

</header>

<main role=\"main\">

  <div class=\"";
    // line 53
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["b5_top_container"] ?? null), 53, $this->source), "html", null, true);
    echo "\">
    ";
    // line 54
    if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "breadcrumb", [], "any", false, false, true, 54)) {
      // line 55
      echo "      ";
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "breadcrumb", [], "any", false, false, true, 55), 55, $this->source), "html", null, true);
      echo "
    ";
    }
    // line 57
    echo "    <div class=\"row g-0\">
      ";
    // line 58
    if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 58)) {
      // line 59
      echo "        <div class=\"order-2 order-lg-1 ";
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebar_first_classes"] ?? null), 59, $this->source), "html", null, true);
      echo "\">
          ";
      // line 60
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 60), 60, $this->source), "html", null, true);
      echo "
        </div>
      ";
    }
    // line 63
    echo "      <div class=\"order-1 order-lg-2 ";
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content_classes"] ?? null), 63, $this->source), "html", null, true);
    echo "\">
        ";
    // line 64
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 64), 64, $this->source), "html", null, true);
    echo "
      </div>
      ";
    // line 66
    if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 66)) {
      // line 67
      echo "        <div class=\"order-3 ";
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebar_second_classes"] ?? null), 67, $this->source), "html", null, true);
      echo "\">
          ";
      // line 68
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 68), 68, $this->source), "html", null, true);
      echo "
        </div>
      ";
    }
    // line 71
    echo "    </div>
  </div>

</main>

";
    // line 76
    $context["footer_top_classes"] = (((" " . (((        // line 77
            ($context["b5_footer_schema"] ?? null) != "none")) ? ((" footer-" . $this->sandbox->ensureToStringAllowed(($context["b5_footer_schema"] ?? null), 77, $this->source))) : (" "))) . (((        // line 78
          ($context["b5_footer_schema"] ?? null) != "none")) ? ((((($context["b5_footer_schema"] ?? null) == "dark")) ? (" text-light") : (" text-dark"))) : (" "))) . (((        // line 79
        ($context["b5_footer_bg_schema"] ?? null) != "none")) ? ((" bg-" . $this->sandbox->ensureToStringAllowed(($context["b5_footer_bg_schema"] ?? null), 79, $this->source))) : (" ")));
    // line 80
    echo "
";
    // line 81
    if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_top", [], "any", false, false, true, 81)) {
      // line 82
      echo "  <footer class=\"mt-auto ";
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_classes"] ?? null), 82, $this->source), "html", null, true);
      echo "\">
    <div class=\"";
      // line 83
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["b5_top_container"] ?? null), 83, $this->source), "html", null, true);
      echo "\">
      ";
      // line 84
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 84), 84, $this->source), "html", null, true);
      echo "
    </div>
  </footer>
";
    }
  }
}
