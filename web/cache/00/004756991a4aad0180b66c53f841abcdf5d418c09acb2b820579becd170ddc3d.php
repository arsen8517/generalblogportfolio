<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/STARTER_SCSS/templates/page/secondBasePage.html.twig */

class __TwigTemplate_2e041eb94a91b0778366e1c4c16c686087caeca10f5b3659cd9c95f440e41592 extends \Twig\Template
{
  private $source;
  private $macros = [];

  public function __construct(Environment $env)
  {
    parent::__construct($env);

    $this->source = $this->getSourceContext();

    $this->parent = false;

    $this->blocks = [
    ];
    $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
    $this->checkSecurity();
  }

  public function getTemplateName()
  {
    return "themes/contrib/glisseo/STARTER_SCSS/templates/page/secondBasePage.html.twig";
  }

  public function isTraitable()
  {
    return false;
  }

  public function getDebugInfo()
  {
    return array(84 => 29, 78 => 26, 74 => 25, 70 => 24, 66 => 23, 61 => 21, 57 => 20, 54 => 19, 52 => 16, 51 => 15, 49 => 11, 48 => 10, 47 => 9, 46 => 8, 45 => 7, 44 => 6, 43 => 5, 41 => 3, 39 => 2,);
  }

  public function getSourceContext()
  {
    return new Source("{# Main lauout classes. #}
{% set main_layout_attributes = create_attribute() %}
{% set main_layout_block = 'main-layout' %}
{%
  set main_layout_modifiers = [
    page.sidebar_first ? 'sidebar-first',
    page.sidebar_second ? 'sidebar-second',
    not page.sidebar_first and not page.sidebar_second ? 'no-sidebars',
    not page.sidebar_first and page.sidebar_second ? 'one-sidebar',
    page.sidebar_first and not page.sidebar_second ? 'one-sidebar',
    page.sidebar_first and page.sidebar_second ? 'two-sidebars',
  ]
%}
{%
  set main_layout_classes = [
    bem(main_layout_block, null, main_layout_modifiers),
  ]
%}

{{ page.header }}
{{ page.navigation }}

<main{{ main_layout_attributes.addClass('layout-container').addClass(main_layout_classes) }}>
  {{ page.sidebar_first }}
  {{ page.content }}
  {{ page.sidebar_second }}
</main>

{{ page.footer }}
", "themes/contrib/glisseo/STARTER_SCSS/templates/page/secondBasePage.html.twig", "/app/web/themes/contrib/glisseo/STARTER_SCSS/templates/page/secondBasePage.html.twig");
  }

  public function checkSecurity()
  {
    static $tags = array("set" => 2);
    static $filters = array("escape" => 20);
    static $functions = array("create_attribute" => 2, "bem" => 16);

    try {
      $this->sandbox->checkSecurity(
        ['set'],
        ['escape'],
        ['create_attribute', 'bem']
      );
    } catch (SecurityError $e) {
      $e->setSourceContext($this->source);

      if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
        $e->setTemplateLine($tags[$e->getTagName()]);
      } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
        $e->setTemplateLine($filters[$e->getFilterName()]);
      } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
        $e->setTemplateLine($functions[$e->getFunctionName()]);
      }

      throw $e;
    }

  }

  protected function doDisplay(array $context, array $blocks = [])
  {
    $macros = $this->macros;
    // line 2
    $context["main_layout_attributes"] = $this->extensions['Drupal\Core\Template\TwigExtension']->createAttribute();
    // line 3
    $context["main_layout_block"] = "main-layout";
    // line 5
    $context["main_layout_modifiers"] = [0 => ((twig_get_attribute($this->env, $this->source,         // line 6
      ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 6)) ? ("sidebar-first") : ("")), 1 => ((twig_get_attribute($this->env, $this->source,         // line 7
      ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 7)) ? ("sidebar-second") : ("")), 2 => (((!twig_get_attribute($this->env, $this->source,         // line 8
        ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 8) && !twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 8))) ? ("no-sidebars") : ("")), 3 => (((!twig_get_attribute($this->env, $this->source,         // line 9
        ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 9) && twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 9))) ? ("one-sidebar") : ("")), 4 => (((twig_get_attribute($this->env, $this->source,         // line 10
        ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 10) && !twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 10))) ? ("one-sidebar") : ("")), 5 => (((twig_get_attribute($this->env, $this->source,         // line 11
        ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 11) && twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 11))) ? ("two-sidebars") : (""))];
    // line 15
    $context["main_layout_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 16
      ($context["main_layout_block"] ?? null), 16, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["main_layout_modifiers"] ?? null), 16, $this->source))];
    // line 19
    echo "
";
    // line 20
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header", [], "any", false, false, true, 20), 20, $this->source), "html", null, true);
    echo "
";
    // line 21
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "navigation", [], "any", false, false, true, 21), 21, $this->source), "html", null, true);
    echo "

<main";
    // line 23
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["main_layout_attributes"] ?? null), "addClass", [0 => "layout-container"], "method", false, false, true, 23), "addClass", [0 => ($context["main_layout_classes"] ?? null)], "method", false, false, true, 23), 23, $this->source), "html", null, true);
    echo ">
  ";
    // line 24
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 24), 24, $this->source), "html", null, true);
    echo "
  ";
    // line 25
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 25), 25, $this->source), "html", null, true);
    echo "
  ";
    // line 26
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 26), 26, $this->source), "html", null, true);
    echo "
</main>

";
    // line 29
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 29), 29, $this->source), "html", null, true);
    echo "
";
  }
}
