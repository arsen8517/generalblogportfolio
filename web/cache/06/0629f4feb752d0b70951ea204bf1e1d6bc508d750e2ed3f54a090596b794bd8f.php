<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/generalblog/templates/home_page/field--header.html.twig */
class __TwigTemplate_3a8e7754024f213b9e55c053ca1d94235d7c68ca26b751293267f33fa5ba63f6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["classes"] = [0 => "field", 1 => ("field--name-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 3
($context["field_name"] ?? null), 3, $this->source))), 2 => ("field--type-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 4
($context["field_type"] ?? null), 4, $this->source))), 3 => ("field--label-" . $this->sandbox->ensureToStringAllowed(        // line 5
($context["label_display"] ?? null), 5, $this->source)), 4 => (((        // line 6
($context["label_display"] ?? null) == "inline")) ? ("clearfix") : (""))];
        // line 8
        $context["title_classes"] = [0 => "field__label", 1 => (((        // line 10
($context["label_display"] ?? null) == "visually_hidden")) ? ("visually-hidden") : (""))];
        // line 12
        echo "
";
        // line 13
        if (($context["label_hidden"] ?? null)) {
            // line 14
            echo "  <div";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null), 1 => "field__items"], "method", false, false, true, 14), 14, $this->source), "html", null, true);
            echo ">
    kkkk
  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/custom/generalblog/templates/home_page/field--header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 14,  51 => 13,  48 => 12,  46 => 10,  45 => 8,  43 => 6,  42 => 5,  41 => 4,  40 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set classes = [
  'field',
  'field--name-' ~ field_name|clean_class,
  'field--type-' ~ field_type|clean_class,
  'field--label-' ~ label_display,
  label_display == 'inline' ? 'clearfix',
] %}
{% set title_classes = [
  'field__label',
  label_display == 'visually_hidden' ? 'visually-hidden',
] %}

{% if label_hidden %}
  <div{{ attributes.addClass(classes, 'field__items') }}>
    kkkk
  </div>
{% endif %}
", "themes/custom/generalblog/templates/home_page/field--header.html.twig", "/app/web/themes/custom/generalblog/templates/home_page/field--header.html.twig");
    }

    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 13);
        static $filters = array("clean_class" => 3, "escape" => 14);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
