<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/content/media/media.html.twig */
class __TwigTemplate_043bcb8bc23407b5e0780661bd996c272ee9f137246e5e6da90920a1fe3a8e69 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'media' => [$this, 'block_media'],
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["block"] = _glisseo_bem_block([0 => twig_get_attribute($this->env, $this->source,         // line 3
($context["media"] ?? null), "getEntityTypeId", [], "method", false, false, true, 3), 1 => (((twig_get_attribute($this->env, $this->source,         // line 4
($context["media"] ?? null), "bundle", [], "method", false, false, true, 4) != twig_get_attribute($this->env, $this->source, ($context["media"] ?? null), "getEntityTypeId", [], "method", false, false, true, 4))) ? (twig_get_attribute($this->env, $this->source, ($context["media"] ?? null), "bundle", [], "method", false, false, true, 4)) : ("")), 2 => (((        // line 5
($context["view_mode"] ?? null) != "default")) ? (($context["view_mode"] ?? null)) : (""))]);
        // line 9
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 10
($context["block"] ?? null), 10, $this->source))];
        // line 13
        echo "<article";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 13), 13, $this->source), "html", null, true);
        echo ">
  ";
        // line 14
        $this->displayBlock('media', $context, $blocks);
        // line 22
        echo "</article>
";
    }

    // line 14
    public function block_media($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "    ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["title_suffix"] ?? null), "contextual_links", [], "any", false, false, true, 15), 15, $this->source), "html", null, true);
        echo "
    ";
        // line 16
        if (($context["content"] ?? null)) {
            // line 17
            echo "      ";
            $this->displayBlock('content', $context, $blocks);
            // line 20
            echo "    ";
        }
        // line 21
        echo "  ";
    }

    // line 17
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null), 18, $this->source), "html", null, true);
        echo "
      ";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/content/media/media.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 18,  82 => 17,  78 => 21,  75 => 20,  72 => 17,  70 => 16,  65 => 15,  61 => 14,  56 => 22,  54 => 14,  49 => 13,  47 => 10,  46 => 9,  44 => 5,  43 => 4,  42 => 3,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{%
  set block = bem_block([
    media.getEntityTypeId(),
    media.bundle() != media.getEntityTypeId() ? media.bundle(),
    view_mode != 'default' ? view_mode,
  ])
%}
{%
  set classes = [
    bem(block),
  ]
%}
<article{{ attributes.addClass(classes) }}>
  {% block media %}
    {{ title_suffix.contextual_links }}
    {% if content %}
      {% block content %}
        {{ content }}
      {% endblock %}
    {% endif %}
  {% endblock media %}
</article>
", "themes/contrib/glisseo/templates/content/media/media.html.twig", "/app/web/themes/contrib/glisseo/templates/content/media/media.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 2, "block" => 14, "if" => 16);
        static $filters = array("escape" => 13);
        static $functions = array("bem_block" => 2, "bem" => 10);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['escape'],
                ['bem_block', 'bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
