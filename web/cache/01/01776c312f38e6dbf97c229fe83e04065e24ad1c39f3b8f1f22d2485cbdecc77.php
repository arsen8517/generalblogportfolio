<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/region/region--sidebar-first.html.twig */

class __TwigTemplate_814426562f8c42ba4d75f0c9f2bc57b8c33fefbca0ba380db3ddc4822c2171df extends \Twig\Template
{
  private $source;
  private $macros = [];

  public function __construct(Environment $env)
  {
    parent::__construct($env);

    $this->source = $this->getSourceContext();

    $this->blocks = [
      'region' => [$this, 'block_region'],
    ];
    $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
    $this->checkSecurity();
  }

  public function block_region($context, array $blocks = [])
  {
    $macros = $this->macros;
    // line 4
    echo "  <aside";
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 4), 4, $this->source), "html", null, true);
    echo ">
    ";
    // line 5
    $this->displayBlock("content", $context, $blocks);
    echo "
  </aside>
";
  }

  public function getTemplateName()
  {
    return "themes/contrib/glisseo/templates/region/region--sidebar-first.html.twig";
  }

  // line 3

  public function isTraitable()
  {
    return false;
  }

  public function getDebugInfo()
  {
    return array(57 => 5, 52 => 4, 48 => 3, 37 => 1,);
  }

  public function getSourceContext()
  {
    return new Source("{% extends 'region--header.html.twig' %}

{% block region %}
  <aside{{ attributes.addClass(classes) }}>
    {{ block('content') }}
  </aside>
{% endblock %}
", "themes/contrib/glisseo/templates/region/region--sidebar-first.html.twig", "/app/web/themes/contrib/glisseo/templates/region/region--sidebar-first.html.twig");
  }

  public function checkSecurity()
  {
    static $tags = array();
    static $filters = array("escape" => 4);
    static $functions = array();

    try {
      $this->sandbox->checkSecurity(
        [],
        ['escape'],
        []
      );
    } catch (SecurityError $e) {
      $e->setSourceContext($this->source);

      if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
        $e->setTemplateLine($tags[$e->getTagName()]);
      } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
        $e->setTemplateLine($filters[$e->getFilterName()]);
      } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
        $e->setTemplateLine($functions[$e->getFunctionName()]);
      }

      throw $e;
    }

  }

  protected function doGetParent(array $context)
  {
    // line 1
    return "region--header.html.twig";
  }

  protected function doDisplay(array $context, array $blocks = [])
  {
    $macros = $this->macros;
    $this->parent = $this->loadTemplate("region--header.html.twig", "themes/contrib/glisseo/templates/region/region--sidebar-first.html.twig", 1);
    $this->parent->display($context, array_merge($this->blocks, $blocks));
  }
}
