<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/content/block/block--system-branding-block.html.twig */
class __TwigTemplate_10ed4fd0632f9ae2669064ac6ea5f34881d30d727bb60804c2f571f04adfa85d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        $context["block"] = (($context["block"]) ?? ("branding"));
        // line 17
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 18
($context["block"] ?? null), 18, $this->source))];
        // line 21
        echo "<div";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 21), 21, $this->source), "html", null, true);
        echo ">
  ";
        // line 22
        if (($context["site_logo"] ?? null)) {
            // line 23
            echo "    <a href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("<front>"));
            echo "\" rel=\"home\" class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 23, $this->source), "logo"), "html", null, true);
            echo "\">
      <img src=\"";
            // line 24
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["site_logo"] ?? null), 24, $this->source), "html", null, true);
            echo "\" alt=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Home"));
            echo "\" class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 24, $this->source), "img"), "html", null, true);
            echo "\" />
    </a>
  ";
        }
        // line 27
        echo "  ";
        if (($context["site_name"] ?? null)) {
            // line 28
            echo "    <a href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("<front>"));
            echo "\" rel=\"home\" class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 28, $this->source), "name"), "html", null, true);
            echo "\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["site_name"] ?? null), 28, $this->source), "html", null, true);
            echo "</a>
  ";
        }
        // line 30
        echo "  ";
        if (($context["site_slogan"] ?? null)) {
            // line 31
            echo "    <span class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 31, $this->source), "slogan"), "html", null, true);
            echo "\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["site_slogan"] ?? null), 31, $this->source), "html", null, true);
            echo "</span>
  ";
        }
        // line 33
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/content/block/block--system-branding-block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 33,  84 => 31,  81 => 30,  71 => 28,  68 => 27,  58 => 24,  51 => 23,  49 => 22,  44 => 21,  42 => 18,  41 => 17,  39 => 15,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Theme override for a branding block.
 *
 * Each branding element variable (logo, name, slogan) is only available if
 * enabled in the block configuration.
 *
 * Available variables:
 * - site_logo: Logo for site as defined in Appearance or theme settings.
 * - site_name: Name for site as defined in Site information settings.
 * - site_slogan: Slogan for site as defined in Site information settings.
 */
#}
{% set block = block ?? 'branding' %}
{%
  set classes = [
    bem(block),
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% if site_logo %}
    <a href=\"{{ path('<front>') }}\" rel=\"home\" class=\"{{ bem(block, 'logo') }}\">
      <img src=\"{{ site_logo }}\" alt=\"{{ 'Home'|t }}\" class=\"{{ bem(block, 'img') }}\" />
    </a>
  {% endif %}
  {% if site_name %}
    <a href=\"{{ path('<front>') }}\" rel=\"home\" class=\"{{ bem(block, 'name') }}\">{{ site_name }}</a>
  {% endif %}
  {% if site_slogan %}
    <span class=\"{{ bem(block, 'slogan') }}\">{{ site_slogan }}</span>
  {% endif %}
</div>
", "themes/contrib/glisseo/templates/content/block/block--system-branding-block.html.twig", "/app/web/themes/contrib/glisseo/templates/content/block/block--system-branding-block.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 15, "if" => 22);
        static $filters = array("escape" => 21, "t" => 24);
        static $functions = array("bem" => 18, "path" => 23);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape', 't'],
                ['bem', 'path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
