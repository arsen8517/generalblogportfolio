<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/form/form-element.html.twig */
class __TwigTemplate_ec5d1d15a5c1e68bb8f708e4dfc560cc647c0bfa36cff3bf6d3ffff6f9b3dba0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        $context["block"] = (($context["block"]) ?? ("form-item"));
        // line 15
        $context["modifiers"] = [0 => ("name-" . $this->sandbox->ensureToStringAllowed(        // line 16
($context["name"] ?? null), 16, $this->source)), 1 => ("type-" . $this->sandbox->ensureToStringAllowed(        // line 17
($context["type"] ?? null), 17, $this->source)), 2 => ((twig_in_filter(        // line 18
($context["type"] ?? null), [0 => "checkbox", 1 => "radio"])) ? ("boolean") : ("")), 3 => ((!twig_in_filter(        // line 19
($context["title_display"] ?? null), [0 => "after", 1 => "before"])) ? ("no-label") : ("")), 4 => (((        // line 20
($context["disabled"] ?? null) == "disabled")) ? ("disabled") : ("")), 5 => ((        // line 21
($context["errors"] ?? null)) ? ("error") : (""))];
        // line 25
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 26
($context["block"] ?? null), 26, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["modifiers"] ?? null), 26, $this->source)), 1 => "js-form-item", 2 => ("js-form-type-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 28
($context["type"] ?? null), 28, $this->source))), 3 => ("js-form-item-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 29
($context["name"] ?? null), 29, $this->source)))];
        // line 33
        $context["description_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 34
($context["block"] ?? null), 34, $this->source), "description"), 1 => (((        // line 35
($context["description_display"] ?? null) == "invisible")) ? ("visually-hidden") : (""))];
        // line 39
        $context["prefix_modifiers"] = [0 => (((        // line 40
($context["disabled"] ?? null) == "disabled")) ? ("disabled") : (""))];
        // line 44
        $context["suffix_modifiers"] = [0 => (((        // line 45
($context["disabled"] ?? null) == "disabled")) ? ("disabled") : (""))];
        // line 48
        echo "<div";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 48), 48, $this->source), "html", null, true);
        echo ">
  ";
        // line 49
        if (twig_in_filter(($context["label_display"] ?? null), [0 => "before", 1 => "invisible"])) {
            // line 50
            echo "    ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 50, $this->source), "html", null, true);
            echo "
  ";
        }
        // line 52
        echo "  ";
        if ( !twig_test_empty(($context["prefix"] ?? null))) {
            // line 53
            echo "    <span class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 53, $this->source), "prefix", $this->sandbox->ensureToStringAllowed(($context["prefix_modifiers"] ?? null), 53, $this->source)), "html", null, true);
            echo "\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["prefix"] ?? null), 53, $this->source), "html", null, true);
            echo "</span>
  ";
        }
        // line 55
        echo "  ";
        if (((($context["description_display"] ?? null) == "before") && twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "content", [], "any", false, false, true, 55))) {
            // line 56
            echo "    <div";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "attributes", [], "any", false, false, true, 56), "addClass", [0 => ($context["description_classes"] ?? null)], "method", false, false, true, 56), 56, $this->source), "html", null, true);
            echo ">
      ";
            // line 57
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "content", [], "any", false, false, true, 57), 57, $this->source), "html", null, true);
            echo "
    </div>
  ";
        }
        // line 60
        echo "  ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["children"] ?? null), 60, $this->source), "html", null, true);
        echo "
  ";
        // line 61
        if ( !twig_test_empty(($context["suffix"] ?? null))) {
            // line 62
            echo "    <span class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 62, $this->source), "prefix", $this->sandbox->ensureToStringAllowed(($context["prefix_modifiers"] ?? null), 62, $this->source)), "html", null, true);
            echo "\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["suffix"] ?? null), 62, $this->source), "html", null, true);
            echo "</span>
  ";
        }
        // line 64
        echo "  ";
        if ((($context["label_display"] ?? null) == "after")) {
            // line 65
            echo "    ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 65, $this->source), "html", null, true);
            echo "
  ";
        }
        // line 67
        echo "  ";
        if (($context["errors"] ?? null)) {
            // line 68
            echo "    <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 68, $this->source), "error-message"), "html", null, true);
            echo "\">
      ";
            // line 69
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["errors"] ?? null), 69, $this->source), "html", null, true);
            echo "
    </div>
  ";
        }
        // line 72
        echo "  ";
        if ((twig_in_filter(($context["description_display"] ?? null), [0 => "after", 1 => "invisible"]) && twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "content", [], "any", false, false, true, 72))) {
            // line 73
            echo "    <div";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "attributes", [], "any", false, false, true, 73), "addClass", [0 => ($context["description_classes"] ?? null)], "method", false, false, true, 73), 73, $this->source), "html", null, true);
            echo ">
      ";
            // line 74
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "content", [], "any", false, false, true, 74), 74, $this->source), "html", null, true);
            echo "
    </div>
  ";
        }
        // line 77
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/form/form-element.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 77,  148 => 74,  143 => 73,  140 => 72,  134 => 69,  129 => 68,  126 => 67,  120 => 65,  117 => 64,  109 => 62,  107 => 61,  102 => 60,  96 => 57,  91 => 56,  88 => 55,  80 => 53,  77 => 52,  71 => 50,  69 => 49,  64 => 48,  62 => 45,  61 => 44,  59 => 40,  58 => 39,  56 => 35,  55 => 34,  54 => 33,  52 => 29,  51 => 28,  50 => 26,  49 => 25,  47 => 21,  46 => 20,  45 => 19,  44 => 18,  43 => 17,  42 => 16,  41 => 15,  39 => 13,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Theme override for a form element.
 *
 * @see template_preprocess_form_element()
 */
#}
{#
  Most of core-provided js assumes that the CSS class pattern js-form-item-[something] or
  js-form-type-[something] exists on form items. We have to keep them.
#}
{% set block = block ?? 'form-item' %}
{%
  set modifiers = [
    'name-' ~ name,
    'type-' ~ type,
    type in ['checkbox', 'radio'] ? 'boolean',
    title_display not in ['after', 'before'] ? 'no-label',
    disabled == 'disabled' ? 'disabled',
    errors ? 'error'
  ]
%}
{%
  set classes = [
    bem(block, null, modifiers),
    'js-form-item',
    'js-form-type-' ~ type|clean_class,
    'js-form-item-' ~ name|clean_class,
  ]
%}
{%
  set description_classes = [
    bem(block, 'description'),
    description_display == 'invisible' ? 'visually-hidden',
  ]
%}
{%
  set prefix_modifiers = [
    disabled == 'disabled' ? 'disabled',
  ]
%}
{%
  set suffix_modifiers = [
    disabled == 'disabled' ? 'disabled',
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% if label_display in ['before', 'invisible'] %}
    {{ label }}
  {% endif %}
  {% if prefix is not empty %}
    <span class=\"{{ bem(block, 'prefix', prefix_modifiers) }}\">{{ prefix }}</span>
  {% endif %}
  {% if description_display == 'before' and description.content %}
    <div{{ description.attributes.addClass(description_classes) }}>
      {{ description.content }}
    </div>
  {% endif %}
  {{ children }}
  {% if suffix is not empty %}
    <span class=\"{{ bem(block, 'prefix', prefix_modifiers) }}\">{{ suffix }}</span>
  {% endif %}
  {% if label_display == 'after' %}
    {{ label }}
  {% endif %}
  {% if errors %}
    <div class=\"{{ bem(block, 'error-message') }}\">
      {{ errors }}
    </div>
  {% endif %}
  {% if description_display in ['after', 'invisible'] and description.content %}
    <div{{ description.attributes.addClass(description_classes) }}>
      {{ description.content }}
    </div>
  {% endif %}
</div>
", "themes/contrib/glisseo/templates/form/form-element.html.twig", "/app/web/themes/contrib/glisseo/templates/form/form-element.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 13, "if" => 49);
        static $filters = array("clean_class" => 28, "escape" => 48);
        static $functions = array("bem" => 26);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['clean_class', 'escape'],
                ['bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
