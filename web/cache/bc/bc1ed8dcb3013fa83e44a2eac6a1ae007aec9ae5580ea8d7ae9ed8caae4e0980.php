<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/scholar_new/templates/layout/region--header.html.twig */

class __TwigTemplate_374bd25f13f5df913b755f87cef688f7009a15e72e617605824c91f702c830e0 extends \Twig\Template
{
  private $source;
  private $macros = [];

  public function __construct(Environment $env)
  {
    parent::__construct($env);

    $this->source = $this->getSourceContext();

    $this->parent = false;

    $this->blocks = [
    ];
    $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
    $this->checkSecurity();
  }

  public function getTemplateName()
  {
    return "themes/custom/scholar_new/templates/layout/region--header.html.twig";
  }

  public function isTraitable()
  {
    return false;
  }

  public function getDebugInfo()
  {
    return array(53 => 24, 48 => 23, 46 => 22, 44 => 19, 43 => 17, 39 => 15,);
  }

  public function getSourceContext()
  {
    return new Source("{#
/**
 * @file
 * Theme override to display a region.
 *
 * Available variables:
 * - content: The content for this region, typically blocks.
 * - attributes: HTML attributes for the region <div>.
 * - region: The name of the region variable as defined in the theme's
 *   .info.yml file.
 *
 * @see template_preprocess_region()
 */
#}
{{ attach_library('scholar_new/landing-page') }}
{%
  set classes = [
    'region',
    'region-' ~ region|clean_class,
  ]
%}
{% if content %}
  <div{{ attributes.addClass(classes) }}>
    {{ content }}
  </div>
{% endif %}
", "themes/custom/scholar_new/templates/layout/region--header.html.twig", "/app/web/themes/custom/scholar_new/templates/layout/region--header.html.twig");
  }

  public function checkSecurity()
  {
    static $tags = array("set" => 17, "if" => 22);
    static $filters = array("escape" => 15, "clean_class" => 19);
    static $functions = array("attach_library" => 15);

    try {
      $this->sandbox->checkSecurity(
        ['set', 'if'],
        ['escape', 'clean_class'],
        ['attach_library']
      );
    } catch (SecurityError $e) {
      $e->setSourceContext($this->source);

      if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
        $e->setTemplateLine($tags[$e->getTagName()]);
      } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
        $e->setTemplateLine($filters[$e->getFilterName()]);
      } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
        $e->setTemplateLine($functions[$e->getFunctionName()]);
      }

      throw $e;
    }

  }

  protected function doDisplay(array $context, array $blocks = [])
  {
    $macros = $this->macros;
    // line 15
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->attachLibrary("scholar_new/landing-page"), "html", null, true);
    echo "
";
    // line 17
    $context["classes"] = [0 => "region", 1 => ("region-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 19
        ($context["region"] ?? null), 19, $this->source)))];
    // line 22
    if (($context["content"] ?? null)) {
      // line 23
      echo "  <div";
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 23), 23, $this->source), "html", null, true);
      echo ">
    ";
      // line 24
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null), 24, $this->source), "html", null, true);
      echo "
  </div>
";
    }
  }
}
