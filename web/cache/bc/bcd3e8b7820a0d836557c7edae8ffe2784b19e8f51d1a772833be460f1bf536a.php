<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/navigation/menu-local-task.html.twig */
class __TwigTemplate_3a9706ec309de4b132dadccf50c78b1f7ffb7ca1fc1752d05bcbc0153b407442 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["block"] = (($context["block"]) ?? ("local-tasks"));
        // line 3
        $context["modifiers"] = [0 => ((        // line 4
($context["is_active"] ?? null)) ? ("active") : (""))];
        // line 8
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 9
($context["block"] ?? null), 9, $this->source), "item", $this->sandbox->ensureToStringAllowed(($context["modifiers"] ?? null), 9, $this->source))];
        // line 13
        $context["link"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["link"] ?? null), 13, $this->source), ["#options" => ["attributes" => ["class" => [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 17
($context["block"] ?? null), 17, $this->source), "link", $this->sandbox->ensureToStringAllowed(($context["modifiers"] ?? null), 17, $this->source))]]]]);
        // line 23
        echo "<li";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 23), 23, $this->source), "html", null, true);
        echo ">";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["link"] ?? null), 23, $this->source), "html", null, true);
        echo "</li>
";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/navigation/menu-local-task.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 23,  48 => 17,  47 => 13,  45 => 9,  44 => 8,  42 => 4,  41 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set block = block ?? 'local-tasks' %}
{%
  set modifiers = [
    is_active ? 'active',
  ]
%}
{%
  set classes = [
    bem(block, 'item', modifiers),
  ]
%}
{%
  set link = link|merge({
    '#options': {
      'attributes': {
        'class': [
          bem(block, 'link', modifiers),
        ]
      }
    }
  })
%}
<li{{ attributes.addClass(classes) }}>{{ link }}</li>
", "themes/contrib/glisseo/templates/navigation/menu-local-task.html.twig", "/app/web/themes/contrib/glisseo/templates/navigation/menu-local-task.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1);
        static $filters = array("merge" => 13, "escape" => 23);
        static $functions = array("bem" => 9);

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['merge', 'escape'],
                ['bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
