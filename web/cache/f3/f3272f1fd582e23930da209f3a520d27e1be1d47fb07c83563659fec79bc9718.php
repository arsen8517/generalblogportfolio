<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/navigation/pager.html.twig */
class __TwigTemplate_69592e57affffe8ad76ce410b5ebcbb9c426e843bd44bd07ab1aa3af21e50684 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        $context["block"] = (($context["block"]) ?? ("pager"));
        // line 38
        if (($context["items"] ?? null)) {
            // line 39
            echo "  <nav class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 39, $this->source)), "html", null, true);
            echo "\" aria-labelledby=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["heading_id"] ?? null), 39, $this->source), "html", null, true);
            echo "\">
    <h4 id=\"";
            // line 40
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["heading_id"] ?? null), 40, $this->source), "html", null, true);
            echo "\" class=\"visually-hidden\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Pagination"));
            echo "</h4>
    <ul class=\"";
            // line 41
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 41, $this->source), "items"), "html", null, true);
            echo " js-pager__items\">
      ";
            // line 43
            echo "      ";
            if (twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "first", [], "any", false, false, true, 43)) {
                // line 44
                echo "        ";
                ob_start();
                // line 45
                echo "        <li class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 45, $this->source), "item", [0 => "action", 1 => "first"]), "html", null, true);
                echo "\">
          <a href=\"";
                // line 46
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "first", [], "any", false, false, true, 46), "href", [], "any", false, false, true, 46), 46, $this->source), "html", null, true);
                echo "\" class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 46, $this->source), "link", [0 => "action-link"]), "html", null, true);
                echo "\" title=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Go to first page"));
                echo "\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->withoutFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "first", [], "any", false, false, true, 46), "attributes", [], "any", false, false, true, 46), 46, $this->source), "href", "title"), "html", null, true);
                echo ">
            <span class=\"visually-hidden\">";
                // line 47
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("First page"));
                echo "</span>
            <span class=\"";
                // line 48
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 48, $this->source), "item-title", [0 => "backwards"]), "html", null, true);
                echo "\" aria-hidden=\"true\">
              ";
                // line 49
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_replace_filter(((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "first", [], "any", false, true, true, 49), "text", [], "any", true, true, true, 49)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "first", [], "any", false, true, true, 49), "text", [], "any", false, false, true, 49), 49, $this->source), t("First"))) : (t("First"))), ["«" => ""]), "html", null, true);
                echo "
            </span>
          </a>
        </li>
        ";
                $___internal_0d4b17f12c3630ed021aa64bc9c5bb6e004208453a95e11dd8a4aa7f77a0eff4_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 44
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(twig_spaceless($___internal_0d4b17f12c3630ed021aa64bc9c5bb6e004208453a95e11dd8a4aa7f77a0eff4_));
                // line 54
                echo "      ";
            }
            // line 55
            echo "
      ";
            // line 57
            echo "      ";
            if (twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "previous", [], "any", false, false, true, 57)) {
                // line 58
                echo "        ";
                ob_start();
                // line 59
                echo "        <li class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 59, $this->source), "item", [0 => "action", 1 => "previous"]), "html", null, true);
                echo "\">
          <a href=\"";
                // line 60
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "previous", [], "any", false, false, true, 60), "href", [], "any", false, false, true, 60), 60, $this->source), "html", null, true);
                echo "\" class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 60, $this->source), "link", [0 => "action-link"]), "html", null, true);
                echo "\" title=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Go to previous page"));
                echo "\" rel=\"prev\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->withoutFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "previous", [], "any", false, false, true, 60), "attributes", [], "any", false, false, true, 60), 60, $this->source), "href", "title", "rel"), "html", null, true);
                echo ">
            <span class=\"visually-hidden\">";
                // line 61
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Previous page"));
                echo "</span>
            <span class=\"";
                // line 62
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 62, $this->source), "item-title", [0 => "backwards"]), "html", null, true);
                echo "\" aria-hidden=\"true\">
              ";
                // line 63
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_replace_filter(((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "previous", [], "any", false, true, true, 63), "text", [], "any", true, true, true, 63)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "previous", [], "any", false, true, true, 63), "text", [], "any", false, false, true, 63), 63, $this->source), t("Previous"))) : (t("Previous"))), ["‹" => ""]), "html", null, true);
                echo "
            </span>
          </a>
        </li>
        ";
                $___internal_978eee429eece2efff49ec3c3b060db6a30cafcfba7da53bb8abdb77ba73bfda_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 58
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(twig_spaceless($___internal_978eee429eece2efff49ec3c3b060db6a30cafcfba7da53bb8abdb77ba73bfda_));
                // line 68
                echo "      ";
            }
            // line 69
            echo "
      ";
            // line 71
            echo "      ";
            if (twig_get_attribute($this->env, $this->source, ($context["ellipses"] ?? null), "previous", [], "any", false, false, true, 71)) {
                // line 72
                echo "        <li class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 72, $this->source), "item", [0 => "ellipsis"]), "html", null, true);
                echo "\" role=\"presentation\">&hellip;</li>
      ";
            }
            // line 74
            echo "
      ";
            // line 76
            echo "      ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "pages", [], "any", false, false, true, 76));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 77
                echo "        ";
                ob_start();
                // line 78
                echo "        <li class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 78, $this->source), "item", [0 => (((($context["current"] ?? null) == $context["key"])) ? ("is-active") : ("")), 1 => "number"]), "html", null, true);
                echo "\">
          ";
                // line 79
                if ((($context["current"] ?? null) == $context["key"])) {
                    // line 80
                    echo "            ";
                    $context["title"] = t("Current page");
                    // line 81
                    echo "          ";
                } else {
                    // line 82
                    echo "            ";
                    $context["title"] = t("Go to page @key", ["@key" => $context["key"]]);
                    // line 83
                    echo "          ";
                }
                // line 84
                echo "          <a href=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "href", [], "any", false, false, true, 84), 84, $this->source), "html", null, true);
                echo "\" class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 84, $this->source), "link", [0 => (((($context["current"] ?? null) == $context["key"])) ? ("is-active") : (""))]), "html", null, true);
                echo "\" title=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null), 84, $this->source), "html", null, true);
                echo "\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->withoutFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "attributes", [], "any", false, false, true, 84), 84, $this->source), "href", "title", "class"), "html", null, true);
                echo ">
            <span class=\"visually-hidden\">
              ";
                // line 86
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar((((($context["current"] ?? null) == $context["key"])) ? (t("Current page")) : (t("Page"))));
                echo "
            </span>
            ";
                // line 88
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["key"], 88, $this->source), "html", null, true);
                echo "
          </a>
        </li>
        ";
                $___internal_1d7f81fb74f2a93175d5c4b3c9e535cc90657c52863aff6fee105b543e1ed767_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 77
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(twig_spaceless($___internal_1d7f81fb74f2a93175d5c4b3c9e535cc90657c52863aff6fee105b543e1ed767_));
                // line 92
                echo "      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 93
            echo "
      ";
            // line 95
            echo "      ";
            if (twig_get_attribute($this->env, $this->source, ($context["ellipses"] ?? null), "next", [], "any", false, false, true, 95)) {
                // line 96
                echo "        <li class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 96, $this->source), "item", [0 => "ellipsis"]), "html", null, true);
                echo "\" role=\"presentation\">&hellip;</li>
      ";
            }
            // line 98
            echo "
      ";
            // line 100
            echo "      ";
            if (twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "next", [], "any", false, false, true, 100)) {
                // line 101
                echo "        ";
                ob_start();
                // line 102
                echo "        <li class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 102, $this->source), "item", [0 => "action", 1 => "next"]), "html", null, true);
                echo "\">
          <a href=\"";
                // line 103
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "next", [], "any", false, false, true, 103), "href", [], "any", false, false, true, 103), 103, $this->source), "html", null, true);
                echo "\" class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 103, $this->source), "link", [0 => "action-link"]), "html", null, true);
                echo "\" title=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Go to next page"));
                echo "\" rel=\"next\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->withoutFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "next", [], "any", false, false, true, 103), "attributes", [], "any", false, false, true, 103), 103, $this->source), "href", "title", "rel"), "html", null, true);
                echo ">
            <span class=\"visually-hidden\">";
                // line 104
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Next page"));
                echo "</span>
            <span class=\"";
                // line 105
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 105, $this->source), "item-title", [0 => "forward"]), "html", null, true);
                echo "\" aria-hidden=\"true\">
              ";
                // line 106
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_replace_filter(((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "next", [], "any", false, true, true, 106), "text", [], "any", true, true, true, 106)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "next", [], "any", false, true, true, 106), "text", [], "any", false, false, true, 106), 106, $this->source), t("Next"))) : (t("Next"))), ["›" => ""]), "html", null, true);
                echo "
            </span>
          </a>
        </li>
        ";
                $___internal_89b2e85806a5603c2fbd824c2c41db5866a9a7cae572b3754f06df45f139b098_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 101
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(twig_spaceless($___internal_89b2e85806a5603c2fbd824c2c41db5866a9a7cae572b3754f06df45f139b098_));
                // line 111
                echo "      ";
            }
            // line 112
            echo "
      ";
            // line 114
            echo "      ";
            if (twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "last", [], "any", false, false, true, 114)) {
                // line 115
                echo "        ";
                ob_start();
                // line 116
                echo "          <li class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 116, $this->source), "item", [0 => "action", 1 => "last"]), "html", null, true);
                echo "\">
          <a href=\"";
                // line 117
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "last", [], "any", false, false, true, 117), "href", [], "any", false, false, true, 117), 117, $this->source), "html", null, true);
                echo "\" class=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 117, $this->source), "link", [0 => "action-link"]), "html", null, true);
                echo "\" title=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Go to last page"));
                echo "\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->withoutFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "last", [], "any", false, false, true, 117), "attributes", [], "any", false, false, true, 117), 117, $this->source), "href", "title"), "html", null, true);
                echo ">
            <span class=\"visually-hidden\">";
                // line 118
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Last page"));
                echo "</span>
            <span class=\"";
                // line 119
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 119, $this->source), "item-title", [0 => "forward"]), "html", null, true);
                echo "\" aria-hidden=\"true\">
              ";
                // line 120
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_replace_filter(((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "last", [], "any", false, true, true, 120), "text", [], "any", true, true, true, 120)) ? (_twig_default_filter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "last", [], "any", false, true, true, 120), "text", [], "any", false, false, true, 120), 120, $this->source), t("Last"))) : (t("Last"))), ["»" => ""]), "html", null, true);
                echo "
            </span>
          </a>
        </li>
        ";
                $___internal_904600fe38a0f610320125cb58eca56d220830273804fc9c657cc15730a818be_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 115
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(twig_spaceless($___internal_904600fe38a0f610320125cb58eca56d220830273804fc9c657cc15730a818be_));
                // line 125
                echo "      ";
            }
            // line 126
            echo "    </ul>
  </nav>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/navigation/pager.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  323 => 126,  320 => 125,  318 => 115,  310 => 120,  306 => 119,  302 => 118,  292 => 117,  287 => 116,  284 => 115,  281 => 114,  278 => 112,  275 => 111,  273 => 101,  265 => 106,  261 => 105,  257 => 104,  247 => 103,  242 => 102,  239 => 101,  236 => 100,  233 => 98,  227 => 96,  224 => 95,  221 => 93,  215 => 92,  213 => 77,  206 => 88,  201 => 86,  189 => 84,  186 => 83,  183 => 82,  180 => 81,  177 => 80,  175 => 79,  170 => 78,  167 => 77,  162 => 76,  159 => 74,  153 => 72,  150 => 71,  147 => 69,  144 => 68,  142 => 58,  134 => 63,  130 => 62,  126 => 61,  116 => 60,  111 => 59,  108 => 58,  105 => 57,  102 => 55,  99 => 54,  97 => 44,  89 => 49,  85 => 48,  81 => 47,  71 => 46,  66 => 45,  63 => 44,  60 => 43,  56 => 41,  50 => 40,  43 => 39,  41 => 38,  39 => 37,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Theme override to display a pager.
 *
 * Available variables:
 * - heading_id: Pagination heading ID.
 * - items: List of pager items.
 *   The list is keyed by the following elements:
 *   - first: Item for the first page; not present on the first page of results.
 *   - previous: Item for the previous page; not present on the first page
 *     of results.
 *   - next: Item for the next page; not present on the last page of results.
 *   - last: Item for the last page; not present on the last page of results.
 *   - pages: List of pages, keyed by page number.
 *   Sub-sub elements:
 *   items.first, items.previous, items.next, items.last, and each item inside
 *   items.pages contain the following elements:
 *   - href: URL with appropriate query parameters for the item.
 *   - attributes: A keyed list of HTML attributes for the item.
 *   - text: The visible text used for the item link, such as \"‹ Previous\"
 *     or \"Next ›\".
 * - current: The page number of the current page.
 * - ellipses: If there are more pages than the quantity allows, then an
 *   ellipsis before or after the listed pages may be present.
 *   - previous: Present if the currently visible list of pages does not start
 *     at the first page.
 *   - next: Present if the visible list of pages ends before the last page.
 *
 * @see template_preprocess_pager()
 *
 * @todo review all uses of the replace() filter below in
 *   https://www.drupal.org/node/3053707 as the behavior it addresses will
 *   likely change when that issue is completed.
 */
#}
{% set block = block ?? 'pager' %}
{% if items %}
  <nav class=\"{{ bem(block) }}\" aria-labelledby=\"{{ heading_id }}\">
    <h4 id=\"{{ heading_id }}\" class=\"visually-hidden\">{{ 'Pagination'|t }}</h4>
    <ul class=\"{{ bem(block, 'items') }} js-pager__items\">
      {# Print first item if we are not on the first page. #}
      {% if items.first %}
        {% apply spaceless %}
        <li class=\"{{ bem(block, 'item', ['action', 'first']) }}\">
          <a href=\"{{ items.first.href }}\" class=\"{{ bem(block, 'link', ['action-link']) }}\" title=\"{{ 'Go to first page'|t }}\"{{ items.first.attributes|without('href', 'title') }}>
            <span class=\"visually-hidden\">{{ 'First page'|t }}</span>
            <span class=\"{{ bem(block, 'item-title', ['backwards']) }}\" aria-hidden=\"true\">
              {{ items.first.text|default('First'|t)|replace({'«': ''}) }}
            </span>
          </a>
        </li>
        {% endapply %}
      {% endif %}

      {# Print previous item if we are not on the first page. #}
      {% if items.previous %}
        {% apply spaceless %}
        <li class=\"{{ bem(block, 'item', ['action', 'previous']) }}\">
          <a href=\"{{ items.previous.href }}\" class=\"{{ bem(block, 'link', ['action-link']) }}\" title=\"{{ 'Go to previous page'|t }}\" rel=\"prev\"{{ items.previous.attributes|without('href', 'title', 'rel') }}>
            <span class=\"visually-hidden\">{{ 'Previous page'|t }}</span>
            <span class=\"{{ bem(block, 'item-title', ['backwards']) }}\" aria-hidden=\"true\">
              {{ items.previous.text|default('Previous'|t)|replace({'‹': ''}) }}
            </span>
          </a>
        </li>
        {% endapply %}
      {% endif %}

      {# Add an ellipsis if there are further previous pages. #}
      {% if ellipses.previous %}
        <li class=\"{{ bem(block, 'item', ['ellipsis']) }}\" role=\"presentation\">&hellip;</li>
      {% endif %}

      {# Now generate the actual pager piece. #}
      {% for key, item in items.pages %}
        {% apply spaceless %}
        <li class=\"{{ bem(block, 'item', [current == key ? 'is-active', 'number']) }}\">
          {% if current == key %}
            {% set title = 'Current page'|t %}
          {% else %}
            {% set title = 'Go to page @key'|t({'@key': key}) %}
          {% endif %}
          <a href=\"{{ item.href }}\" class=\"{{ bem(block, 'link', [current == key ? 'is-active']) }}\" title=\"{{ title }}\"{{ item.attributes|without('href', 'title', 'class') }}>
            <span class=\"visually-hidden\">
              {{ current == key ? 'Current page'|t : 'Page'|t }}
            </span>
            {{ key }}
          </a>
        </li>
        {% endapply %}
      {% endfor %}

      {# Add an ellipsis if there are further next pages. #}
      {% if ellipses.next %}
        <li class=\"{{ bem(block, 'item', ['ellipsis']) }}\" role=\"presentation\">&hellip;</li>
      {% endif %}

      {# Print next item if we are not on the last page. #}
      {% if items.next %}
        {% apply spaceless %}
        <li class=\"{{ bem(block, 'item', ['action', 'next']) }}\">
          <a href=\"{{ items.next.href }}\" class=\"{{ bem(block, 'link', ['action-link']) }}\" title=\"{{ 'Go to next page'|t }}\" rel=\"next\"{{ items.next.attributes|without('href', 'title', 'rel') }}>
            <span class=\"visually-hidden\">{{ 'Next page'|t }}</span>
            <span class=\"{{ bem(block, 'item-title', ['forward']) }}\" aria-hidden=\"true\">
              {{ items.next.text|default('Next'|t)|replace({'›': ''}) }}
            </span>
          </a>
        </li>
        {% endapply %}
      {% endif %}

      {# Print last item if we are not on the last page. #}
      {% if items.last %}
        {% apply spaceless %}
          <li class=\"{{ bem(block, 'item', ['action', 'last']) }}\">
          <a href=\"{{ items.last.href }}\" class=\"{{ bem(block, 'link', ['action-link']) }}\" title=\"{{ 'Go to last page'|t }}\"{{ items.last.attributes|without('href', 'title') }}>
            <span class=\"visually-hidden\">{{ 'Last page'|t }}</span>
            <span class=\"{{ bem(block, 'item-title', ['forward']) }}\" aria-hidden=\"true\">
              {{ items.last.text|default('Last'|t)|replace({'»': ''}) }}
            </span>
          </a>
        </li>
        {% endapply %}
      {% endif %}
    </ul>
  </nav>
{% endif %}
", "themes/contrib/glisseo/templates/navigation/pager.html.twig", "/app/web/themes/contrib/glisseo/templates/navigation/pager.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 37, "if" => 38, "apply" => 44, "for" => 76);
        static $filters = array("escape" => 39, "t" => 40, "without" => 46, "replace" => 49, "default" => 49, "spaceless" => 44);
        static $functions = array("bem" => 39);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'apply', 'for'],
                ['escape', 't', 'without', 'replace', 'default', 'spaceless'],
                ['bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
