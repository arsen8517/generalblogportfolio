<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/generalblog/templates/layout/region--body.html.twig */
class __TwigTemplate_2418f14f2d44d99cd7d33be8dfd1bce5c68e5fc145b31abd8715f13036df2733 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        $context["classes"] = [0 => "body", 1 => "region", 2 => ("region-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 18
($context["region"] ?? null), 18, $this->source)))];
        // line 20
        echo "
";
        // line 21
        if (($context["content"] ?? null)) {
            // line 22
            echo "  <body";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 22), 22, $this->source), "html", null, true);
            echo ">
  ";
            // line 24
            echo "  <div class=\"navigate-main-block-content\">
    <div class=\"nav-left-block\">
      <div class=\"nav-elem-block-up-left\">
        <div class=\"nav-about-me\"><a href=\"#aboutMe\">about me</a></div>
      </div>
      <div class=\"nav-elem-block-center-left\">
        <div class=\"nav-scroll\"><a href=\"#scrollDown\">scroll down</a></div>
      </div>
      <div class=\"nav-elem-block-down-left\">
        <div class=\"ideas\"><a href=\"#ideas\">ideas</a></div>
      </div>
    </div>
    <div class=\"nav-center-block\">
      <div class=\"nav-elem-block-center-first\">
        <img src=\"/themes/custom/generalblog/sources/images/gostinaya_001.jpg\" alt=\"room\" class=\"first-foto-nav\">
      </div>
      <div class=\"nav-elem-block-center-second\">
        <img src=\"/themes/custom/generalblog/sources/images/room_002.jpg\" alt=\"room\" class=\"second-foto-nav\">
      </div>
      <div class=\"nav-elem-block-center-third\">
        <img src=\"/themes/custom/generalblog/sources/images/room_003.jpg\" alt=\"room\" class=\"third-foto-nav\">
      </div>
      <div class=\"nav-elem-block-center-fourth\">
        <img src=\"/themes/custom/generalblog/sources/images/room_004.jpg\" alt=\"room\" class=\"fourth-foto-nav\">
      </div>
      <div class=\"nav-elem-block-center-fifth\">
        <img src=\"/themes/custom/generalblog/sources/images/room_005.jpg\" alt=\"room\" class=\"fifth-foto-nav\">
      </div>
      <div class=\"nav-elem-block-center-sixth\">
        <img src=\"/themes/custom/generalblog/sources/images/room_006.jpg\" alt=\"room\" class=\"six-foto-nav\">
      </div>
    </div>
    <div class=\"nav-right-block\">
      <div class=\"nav-elem-block-up-right\">
        <div class=\"nav-help\"><a href=\"#help\">help</a></div>
      </div>
      <div class=\"nav-elem-block-center-right\">
        <div class=\"nav-connect\"><a href=\"#connect\">connect</a></div>
      </div>
      <div class=\"nav-elem-block-down-right\">
        <div class=\"nav-info\"><a href=\"#info\">info</a></div>
      </div>
    </div>
  </div>
  ";
            // line 69
            echo "  <div class=\"navigate-down-block-content\">
    <div class=\"left-down-block-content\">
      <div class=\"logo-about\">About</div>
      <div class=\"nav-links\">
        <ul class=\"set-ul-nav\">
          <li class=\"nav-set\"><a>about /</a></li>
          <li class=\"nav-set\"><a>work /</a></li>
          <li class=\"nav-set\"><a>contact /</a></li>
          <li class=\"nav-set\"><a>shop</a></li>
        </ul>
      </div>
    </div>
    <div class=\"middle-down-block-content\"></div>
    <div class=\"right-down-block-content\">
      <div class=\"block-content-up\">
        <p id=\"aboutMe\" class=\"title-block-content-up\">I am a UX Designer and Art Director from Austria living in
          Berlin.</p>

        <p class=\"text-block-content-up\">Artworks and illustrations were my gateway to the creative industry which led
          to the foundation of my
          own studio and to first steps in the digital world.</p>
        <p id=\"connect\" class=\"text-block-content-up\">Out of this love for aesthetic design my passion for functionality
          and
          structure evolved. Jumping right into Photoshop didn’t feel accurate anymore and skipping the steps of
          building a framework based on functionality and usability became inevitable.</p>
      </div>
      <div class=\"block-content-center\">
        <p class=\"title-block-content-center\">Art Direction</p></div>
      <p class=\"text-block-content-center\">Starting with basic websites several years ago I found myself working as an
        Art Director for complex projects with a holistic approach soon. Visually appealing designs, subtle details and
        brand guidelines combined to innovative interfaces across various touch points became my daily companion.</p>
      <div id=\"help\" class=\"block-content-down\">
        <p class=\"title-block-content-down\">UX Design</p>
        <p id=\"info\" class=\"text-block-content-down\">A user-centered mindset and sensitivity for design turned out to be
          the
          perfect fit when collaborating with agencies, clients and brands to develop digital concepts and
          solve problems together.</p>
        <div class=\"text-block-content-down\">This rough framework outlines my process of developing digital
          experiences:
          <ol id=\"ideas\" class=\"list-block-content-down\">
            <li>RESEARCH and gather the present state</li>
            <li>STRUCTURE setup and content of the project</li>
            <li>CONCEPT AND STRATEGY</li>
            <li>CREATE, evaluate and iterate deliverables like</li>
            <div class=\"second-list-in-block-content-down\">
              <ul class=\"set-second-list-in-block\">
                <li>Personas</li>
                <li>Site maps</li>
                <li>Use cases and scenarios</li>
                <li>User flow</li>
                <li>Sketches and Infographics</li>
                <li>Wireframes</li>
              </ul>
            </div>
          </ol>
        </div>
        <p class=\"text-block-content-down\">Communicating the conceptual approach/UX strategy and reasoning behind it, is
          present during the whole process.</p>
        <div class=\"image-palm\">
          <div class=\"image\">
            <img src=\"/themes/custom/generalblog/sources/images/fingers-about.jpg\" alt=\"palm\" class=\"set-image\">
          </div>
          <div class=\"description-for-image\">
            <p id=\"scrollDown\" class=\"text-for-description\">To achieve the goal of making the web a better place I
              trust in teamwork,
              latest technology and
              intuition.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  </body>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/custom/generalblog/templates/layout/region--body.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 69,  52 => 24,  47 => 22,  45 => 21,  42 => 20,  40 => 18,  39 => 15,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Theme override to display a region.
 *
 * Available variables:
 * - content: The content for this region, typically blocks.
 * - attributes: HTML attributes for the region <div>.
 * - region: The name of the region variable as defined in the theme's
 *   .info.yml file.
 *
 * @see template_preprocess_region()
 */
#}
{% set classes = [
  'body',
  'region',
  'region-' ~ region|clean_class,
] %}

{% if content %}
  <body{{ attributes.addClass(classes) }}>
  {# <div class=\"wrapper-for-navigate-main-block-content\"> #}
  <div class=\"navigate-main-block-content\">
    <div class=\"nav-left-block\">
      <div class=\"nav-elem-block-up-left\">
        <div class=\"nav-about-me\"><a href=\"#aboutMe\">about me</a></div>
      </div>
      <div class=\"nav-elem-block-center-left\">
        <div class=\"nav-scroll\"><a href=\"#scrollDown\">scroll down</a></div>
      </div>
      <div class=\"nav-elem-block-down-left\">
        <div class=\"ideas\"><a href=\"#ideas\">ideas</a></div>
      </div>
    </div>
    <div class=\"nav-center-block\">
      <div class=\"nav-elem-block-center-first\">
        <img src=\"/themes/custom/generalblog/sources/images/gostinaya_001.jpg\" alt=\"room\" class=\"first-foto-nav\">
      </div>
      <div class=\"nav-elem-block-center-second\">
        <img src=\"/themes/custom/generalblog/sources/images/room_002.jpg\" alt=\"room\" class=\"second-foto-nav\">
      </div>
      <div class=\"nav-elem-block-center-third\">
        <img src=\"/themes/custom/generalblog/sources/images/room_003.jpg\" alt=\"room\" class=\"third-foto-nav\">
      </div>
      <div class=\"nav-elem-block-center-fourth\">
        <img src=\"/themes/custom/generalblog/sources/images/room_004.jpg\" alt=\"room\" class=\"fourth-foto-nav\">
      </div>
      <div class=\"nav-elem-block-center-fifth\">
        <img src=\"/themes/custom/generalblog/sources/images/room_005.jpg\" alt=\"room\" class=\"fifth-foto-nav\">
      </div>
      <div class=\"nav-elem-block-center-sixth\">
        <img src=\"/themes/custom/generalblog/sources/images/room_006.jpg\" alt=\"room\" class=\"six-foto-nav\">
      </div>
    </div>
    <div class=\"nav-right-block\">
      <div class=\"nav-elem-block-up-right\">
        <div class=\"nav-help\"><a href=\"#help\">help</a></div>
      </div>
      <div class=\"nav-elem-block-center-right\">
        <div class=\"nav-connect\"><a href=\"#connect\">connect</a></div>
      </div>
      <div class=\"nav-elem-block-down-right\">
        <div class=\"nav-info\"><a href=\"#info\">info</a></div>
      </div>
    </div>
  </div>
  {# </div> #}
  <div class=\"navigate-down-block-content\">
    <div class=\"left-down-block-content\">
      <div class=\"logo-about\">About</div>
      <div class=\"nav-links\">
        <ul class=\"set-ul-nav\">
          <li class=\"nav-set\"><a>about /</a></li>
          <li class=\"nav-set\"><a>work /</a></li>
          <li class=\"nav-set\"><a>contact /</a></li>
          <li class=\"nav-set\"><a>shop</a></li>
        </ul>
      </div>
    </div>
    <div class=\"middle-down-block-content\"></div>
    <div class=\"right-down-block-content\">
      <div class=\"block-content-up\">
        <p id=\"aboutMe\" class=\"title-block-content-up\">I am a UX Designer and Art Director from Austria living in
          Berlin.</p>

        <p class=\"text-block-content-up\">Artworks and illustrations were my gateway to the creative industry which led
          to the foundation of my
          own studio and to first steps in the digital world.</p>
        <p id=\"connect\" class=\"text-block-content-up\">Out of this love for aesthetic design my passion for functionality
          and
          structure evolved. Jumping right into Photoshop didn’t feel accurate anymore and skipping the steps of
          building a framework based on functionality and usability became inevitable.</p>
      </div>
      <div class=\"block-content-center\">
        <p class=\"title-block-content-center\">Art Direction</p></div>
      <p class=\"text-block-content-center\">Starting with basic websites several years ago I found myself working as an
        Art Director for complex projects with a holistic approach soon. Visually appealing designs, subtle details and
        brand guidelines combined to innovative interfaces across various touch points became my daily companion.</p>
      <div id=\"help\" class=\"block-content-down\">
        <p class=\"title-block-content-down\">UX Design</p>
        <p id=\"info\" class=\"text-block-content-down\">A user-centered mindset and sensitivity for design turned out to be
          the
          perfect fit when collaborating with agencies, clients and brands to develop digital concepts and
          solve problems together.</p>
        <div class=\"text-block-content-down\">This rough framework outlines my process of developing digital
          experiences:
          <ol id=\"ideas\" class=\"list-block-content-down\">
            <li>RESEARCH and gather the present state</li>
            <li>STRUCTURE setup and content of the project</li>
            <li>CONCEPT AND STRATEGY</li>
            <li>CREATE, evaluate and iterate deliverables like</li>
            <div class=\"second-list-in-block-content-down\">
              <ul class=\"set-second-list-in-block\">
                <li>Personas</li>
                <li>Site maps</li>
                <li>Use cases and scenarios</li>
                <li>User flow</li>
                <li>Sketches and Infographics</li>
                <li>Wireframes</li>
              </ul>
            </div>
          </ol>
        </div>
        <p class=\"text-block-content-down\">Communicating the conceptual approach/UX strategy and reasoning behind it, is
          present during the whole process.</p>
        <div class=\"image-palm\">
          <div class=\"image\">
            <img src=\"/themes/custom/generalblog/sources/images/fingers-about.jpg\" alt=\"palm\" class=\"set-image\">
          </div>
          <div class=\"description-for-image\">
            <p id=\"scrollDown\" class=\"text-for-description\">To achieve the goal of making the web a better place I
              trust in teamwork,
              latest technology and
              intuition.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  </body>
{% endif %}
", "themes/custom/generalblog/templates/layout/region--body.html.twig", "/app/web/themes/custom/generalblog/templates/layout/region--body.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 15, "if" => 21);
        static $filters = array("clean_class" => 18, "escape" => 22);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
