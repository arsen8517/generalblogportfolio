<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/navigation/menu.html.twig */
class __TwigTemplate_c7cf81601412dfa7e38f64f8ec3d695948e702ed80b93fbdfde4f8180963e138 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["menus"] = $this->macros["menus"] = $this;
        // line 2
        echo "
";
        // line 7
        $context["block"] = (($context["block"]) ?? ("menu"));
        // line 9
        $context["modifiers"] = [0 =>         // line 10
($context["menu_name"] ?? null)];
        // line 13
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(twig_call_macro($macros["menus"], "macro_menu_links", [($context["items"] ?? null), ($context["attributes"] ?? null), 0, ($context["block"] ?? null), ($context["modifiers"] ?? null)], 13, $context, $this->getSourceContext()));
        echo "

";
    }

    // line 15
    public function macro_menu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, $__block__ = null, $__modifiers__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "block" => $__block__,
            "modifiers" => $__modifiers__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            // line 16
            echo "  ";
            $macros["menus"] = $this;
            // line 17
            echo "  ";
            if (($context["items"] ?? null)) {
                // line 18
                echo "    ";
                if ((($context["menu_level"] ?? null) == 0)) {
                    // line 19
                    echo "      ";
                    $context["classes"] = _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 19, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["modifiers"] ?? null), 19, $this->source));
                    // line 20
                    echo "      <ul";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 20), 20, $this->source), "html", null, true);
                    echo ">
        ";
                    // line 21
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                        // line 22
                        echo "          ";
                        // line 23
                        $context["item_modifiers"] = [0 => ((twig_get_attribute($this->env, $this->source,                         // line 24
$context["item"], "is_expanded", [], "any", false, false, true, 24)) ? ("expanded") : ("")), 1 => ((twig_get_attribute($this->env, $this->source,                         // line 25
$context["item"], "is_collapsed", [], "any", false, false, true, 25)) ? ("collapsed") : ("")), 2 => ((twig_get_attribute($this->env, $this->source,                         // line 26
$context["item"], "in_active_trail", [], "any", false, false, true, 26)) ? ("active-trail") : (""))];
                        // line 29
                        echo "          ";
                        // line 30
                        $context["item_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(                        // line 31
($context["block"] ?? null), 31, $this->source), "item", $this->sandbox->ensureToStringAllowed(($context["item_modifiers"] ?? null), 31, $this->source))];
                        // line 34
                        echo "          <li";
                        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "attributes", [], "any", false, false, true, 34), "addClass", [0 => ($context["item_classes"] ?? null)], "method", false, false, true, 34), 34, $this->source), "html", null, true);
                        echo ">
            ";
                        // line 35
                        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->getLink($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, true, 35), 35, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "url", [], "any", false, false, true, 35), 35, $this->source), ["class" => [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 35, $this->source), "link")]]), "html", null, true);
                        echo "
            ";
                        // line 36
                        if (twig_get_attribute($this->env, $this->source, $context["item"], "below", [], "any", false, false, true, 36)) {
                            // line 37
                            echo "              ";
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(twig_call_macro($macros["menus"], "macro_menu_links", [twig_get_attribute($this->env, $this->source, $context["item"], "below", [], "any", false, false, true, 37), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1), ($context["block"] ?? null)], 37, $context, $this->getSourceContext()));
                            echo "
            ";
                        }
                        // line 39
                        echo "          </li>
        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 41
                    echo "      </ul>
    ";
                } else {
                    // line 43
                    echo "      ";
                    // line 44
                    $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(                    // line 45
($context["block"] ?? null), 45, $this->source), "submenu")];
                    // line 48
                    echo "      <ul";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 48), 48, $this->source), "html", null, true);
                    echo ">
        ";
                    // line 49
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                        // line 50
                        echo "          ";
                        // line 51
                        $context["item_modifiers"] = [0 => "submenu", 1 => ((twig_get_attribute($this->env, $this->source,                         // line 53
$context["item"], "is_expanded", [], "any", false, false, true, 53)) ? ("expanded") : ("")), 2 => ((twig_get_attribute($this->env, $this->source,                         // line 54
$context["item"], "is_collapsed", [], "any", false, false, true, 54)) ? ("collapsed") : ("")), 3 => ((twig_get_attribute($this->env, $this->source,                         // line 55
$context["item"], "in_active_trail", [], "any", false, false, true, 55)) ? ("active-trail") : (""))];
                        // line 58
                        echo "          ";
                        // line 59
                        $context["item_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(                        // line 60
($context["block"] ?? null), 60, $this->source), "item", $this->sandbox->ensureToStringAllowed(($context["item_modifiers"] ?? null), 60, $this->source))];
                        // line 63
                        echo "          <li";
                        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "attributes", [], "any", false, false, true, 63), "addClass", [0 => ($context["item_classes"] ?? null)], "method", false, false, true, 63), 63, $this->source), "html", null, true);
                        echo ">
            ";
                        // line 64
                        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->getLink($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, true, 64), 64, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "url", [], "any", false, false, true, 64), 64, $this->source), ["class" => [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 64, $this->source), "link", [0 => "submenu"])]]), "html", null, true);
                        echo "
            ";
                        // line 65
                        if (twig_get_attribute($this->env, $this->source, $context["item"], "below", [], "any", false, false, true, 65)) {
                            // line 66
                            echo "              ";
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(twig_call_macro($macros["menus"], "macro_menu_links", [twig_get_attribute($this->env, $this->source, $context["item"], "below", [], "any", false, false, true, 66), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1)], 66, $context, $this->getSourceContext()));
                            echo "
            ";
                        }
                        // line 68
                        echo "          </li>
        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 70
                    echo "      </ul>
    ";
                }
                // line 72
                echo "  ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/navigation/menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 72,  184 => 70,  177 => 68,  171 => 66,  169 => 65,  165 => 64,  160 => 63,  158 => 60,  157 => 59,  155 => 58,  153 => 55,  152 => 54,  151 => 53,  150 => 51,  148 => 50,  144 => 49,  139 => 48,  137 => 45,  136 => 44,  134 => 43,  130 => 41,  123 => 39,  117 => 37,  115 => 36,  111 => 35,  106 => 34,  104 => 31,  103 => 30,  101 => 29,  99 => 26,  98 => 25,  97 => 24,  96 => 23,  94 => 22,  90 => 21,  85 => 20,  82 => 19,  79 => 18,  76 => 17,  73 => 16,  56 => 15,  49 => 13,  47 => 10,  46 => 9,  44 => 7,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import _self as menus %}

{#
  We call a macro which calls itself to render the full tree.
  @see http://twig.sensiolabs.org/doc/tags/macro.html
#}
{% set block = block ?? 'menu' %}
{%
  set modifiers = [
    menu_name,
  ]
%}
{{ menus.menu_links(items, attributes, 0, block, modifiers) }}

{% macro menu_links(items, attributes, menu_level, block, modifiers) %}
  {% import _self as menus %}
  {% if items %}
    {% if menu_level == 0 %}
      {% set classes = bem(block, null, modifiers) %}
      <ul{{ attributes.addClass(classes) }}>
        {% for item in items %}
          {%
            set item_modifiers = [
              item.is_expanded ? 'expanded',
              item.is_collapsed ? 'collapsed',
              item.in_active_trail ? 'active-trail',
            ]
          %}
          {%
            set item_classes = [
              bem(block, 'item', item_modifiers),
            ]
          %}
          <li{{ item.attributes.addClass(item_classes) }}>
            {{ link(item.title, item.url, { 'class': [bem(block, 'link')] }) }}
            {% if item.below %}
              {{ menus.menu_links(item.below, attributes, menu_level + 1, block) }}
            {% endif %}
          </li>
        {% endfor %}
      </ul>
    {% else %}
      {%
        set classes = [
          bem(block, 'submenu'),
        ]
      %}
      <ul{{ attributes.addClass(classes) }}>
        {% for item in items %}
          {%
            set item_modifiers = [
              'submenu',
              item.is_expanded ? 'expanded',
              item.is_collapsed ? 'collapsed',
              item.in_active_trail ? 'active-trail',
            ]
          %}
          {%
            set item_classes = [
              bem(block, 'item', item_modifiers),
            ]
          %}
          <li{{ item.attributes.addClass(item_classes) }}>
            {{ link(item.title, item.url, { 'class': [bem(block, 'link', ['submenu'])] }) }}
            {% if item.below %}
              {{ menus.menu_links(item.below, attributes, menu_level + 1) }}
            {% endif %}
          </li>
        {% endfor %}
      </ul>
    {% endif %}
  {% endif %}
{% endmacro %}
", "themes/contrib/glisseo/templates/navigation/menu.html.twig", "/app/web/themes/contrib/glisseo/templates/navigation/menu.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("import" => 1, "set" => 7, "macro" => 15, "if" => 17, "for" => 21);
        static $filters = array("escape" => 20);
        static $functions = array("bem" => 19, "link" => 35);

        try {
            $this->sandbox->checkSecurity(
                ['import', 'set', 'macro', 'if', 'for'],
                ['escape'],
                ['bem', 'link']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
