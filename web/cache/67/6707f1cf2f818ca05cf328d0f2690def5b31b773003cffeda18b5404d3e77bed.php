<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/content/block/block.html.twig */
class __TwigTemplate_1b75cbc090e37d9c40e63c50cad66537de48747d7a870bf0740e32cfec79d148 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'block' => [$this, 'block_block'],
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "removeAttribute", [0 => "id"], "method", false, false, true, 14);
        // line 15
        $context["block"] = (($context["block"]) ?? ("block"));
        // line 17
        $context["modifiers"] = [0 =>         // line 18
($context["block_id"] ?? null), 1 =>         // line 19
($context["block_content_bundle"] ?? null)];
        // line 23
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 24
($context["block"] ?? null), 24, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["modifiers"] ?? null), 24, $this->source))];
        // line 28
        $context["title_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 29
($context["block"] ?? null), 29, $this->source), "title")];
        // line 33
        $context["content_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 34
($context["block"] ?? null), 34, $this->source), "content")];
        // line 37
        echo "<div";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 37), 37, $this->source), "html", null, true);
        echo ">
  ";
        // line 38
        $this->displayBlock('block', $context, $blocks);
        // line 55
        echo "</div>
";
    }

    // line 38
    public function block_block($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "    ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null), 39, $this->source), "html", null, true);
        echo "
    ";
        // line 40
        if (($context["label"] ?? null)) {
            // line 41
            echo "      <div";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["title_attributes"] ?? null), "addClass", [0 => ($context["title_classes"] ?? null)], "method", false, false, true, 41), 41, $this->source), "html", null, true);
            echo ">
        ";
            // line 42
            $this->displayBlock('title', $context, $blocks);
            // line 45
            echo "      </div>
    ";
        }
        // line 47
        echo "    ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null), 47, $this->source), "html", null, true);
        echo "

    <div";
        // line 49
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method", false, false, true, 49), 49, $this->source), "html", null, true);
        echo ">
      ";
        // line 50
        $this->displayBlock('content', $context, $blocks);
        // line 53
        echo "    </div>
  ";
    }

    // line 42
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        echo "          ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 43, $this->source), "html", null, true);
        echo "
        ";
    }

    // line 50
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 51
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null), 51, $this->source), "html", null, true);
        echo "
      ";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/content/block/block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 51,  121 => 50,  114 => 43,  110 => 42,  105 => 53,  103 => 50,  99 => 49,  93 => 47,  89 => 45,  87 => 42,  82 => 41,  80 => 40,  75 => 39,  71 => 38,  66 => 55,  64 => 38,  59 => 37,  57 => 34,  56 => 33,  54 => 29,  53 => 28,  51 => 24,  50 => 23,  48 => 19,  47 => 18,  46 => 17,  44 => 15,  42 => 14,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Theme override to display a block.
 *
 * Available variables:
 * - block_id: The block ID. The block ID will differ depends on what block type
 *   is used.
 * - block_content_bundle: The block bundle if block is content block.
 *
 * @see template_preprocess_block()
 */
#}
{% do attributes.removeAttribute('id') %}
{% set block = block ?? 'block' %}
{%
  set modifiers = [
    block_id,
    block_content_bundle,
  ]
%}
{%
  set classes = [
    bem(block, null, modifiers),
  ]
%}
{%
  set title_classes = [
    bem(block, 'title'),
  ]
%}
{%
  set content_classes = [
    bem(block, 'content'),
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% block block %}
    {{ title_prefix }}
    {% if label %}
      <div{{ title_attributes.addClass(title_classes) }}>
        {% block title %}
          {{ label }}
        {% endblock title %}
      </div>
    {% endif %}
    {{ title_suffix }}

    <div{{ content_attributes.addClass(content_classes) }}>
      {% block content %}
        {{ content }}
      {% endblock content %}
    </div>
  {% endblock block %}
</div>
", "themes/contrib/glisseo/templates/content/block/block.html.twig", "/app/web/themes/contrib/glisseo/templates/content/block/block.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("do" => 14, "set" => 15, "block" => 38, "if" => 40);
        static $filters = array("escape" => 37);
        static $functions = array("bem" => 24);

        try {
            $this->sandbox->checkSecurity(
                ['do', 'set', 'block', 'if'],
                ['escape'],
                ['bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
