<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/content/node/node.html.twig */
class __TwigTemplate_4c8d5ea69940f7f1352969d29812ac588ddd8eb30f03decc16801392800b718e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'node' => [$this, 'block_node'],
            'header' => [$this, 'block_header'],
            'title' => [$this, 'block_title'],
            'submitted' => [$this, 'block_submitted'],
            'content' => [$this, 'block_content'],
            'links' => [$this, 'block_links'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["block"] = _glisseo_bem_block([0 => twig_get_attribute($this->env, $this->source,         // line 3
($context["node"] ?? null), "entityTypeId", [], "method", false, false, true, 3), 1 => twig_get_attribute($this->env, $this->source,         // line 4
($context["node"] ?? null), "bundle", [], "method", false, false, true, 4), 2 => (((        // line 5
($context["view_mode"] ?? null) != "default")) ? (($context["view_mode"] ?? null)) : (""))]);
        // line 9
        $context["modifiers"] = [0 => ((twig_get_attribute($this->env, $this->source,         // line 10
($context["node"] ?? null), "isPromoted", [], "method", false, false, true, 10)) ? ("promoted") : ("")), 1 => ((twig_get_attribute($this->env, $this->source,         // line 11
($context["node"] ?? null), "isSticky", [], "method", false, false, true, 11)) ? ("sticky") : ("")), 2 => (( !twig_get_attribute($this->env, $this->source,         // line 12
($context["node"] ?? null), "isPublished", [], "method", false, false, true, 12)) ? ("unpublished") : (""))];
        // line 16
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 17
($context["block"] ?? null), 17, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["modifiers"] ?? null), 17, $this->source))];
        // line 21
        $context["title_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 22
($context["block"] ?? null), 22, $this->source), "title")];
        // line 26
        $context["author_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 27
($context["block"] ?? null), 27, $this->source), "submitted")];
        // line 31
        $context["content_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 32
($context["block"] ?? null), 32, $this->source), "content")];
        // line 36
        if (twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "hasAttribute", [0 => "role"], "method", false, false, true, 36)) {
            // line 37
            echo "  ";
            twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "removeAttribute", [0 => "role"], "method", false, false, true, 37);
        }
        // line 39
        echo "<article";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 39), 39, $this->source), "html", null, true);
        echo ">
  ";
        // line 40
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null), 40, $this->source), "html", null, true);
        echo "
  ";
        // line 41
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null), 41, $this->source), "html", null, true);
        echo "

  ";
        // line 43
        $this->displayBlock('node', $context, $blocks);
        // line 81
        echo "</article>
";
    }

    // line 43
    public function block_node($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "    ";
        if (($context["display_submitted"] ?? null)) {
            // line 45
            echo "      <header class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 45, $this->source), "header"), "html", null, true);
            echo "\">
        ";
            // line 46
            $this->displayBlock('header', $context, $blocks);
            // line 63
            echo "      </header>
    ";
        }
        // line 65
        echo "
    <div";
        // line 66
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method", false, false, true, 66), 66, $this->source), "html", null, true);
        echo ">
      ";
        // line 67
        $this->displayBlock('content', $context, $blocks);
        // line 70
        echo "    </div>

    ";
        // line 72
        if (twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "links", [], "any", false, false, true, 72)) {
            // line 73
            echo "      <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 73, $this->source), "links"), "html", null, true);
            echo "\">
        ";
            // line 74
            $this->displayBlock('links', $context, $blocks);
            // line 77
            echo "      </div>
    ";
        }
        // line 79
        echo "
  ";
    }

    // line 46
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 47
        echo "          ";
        if ( !($context["page"] ?? null)) {
            // line 48
            echo "            <h2";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["title_attributes"] ?? null), "addClass", [0 => ($context["title_classes"] ?? null)], "method", false, false, true, 48), 48, $this->source), "html", null, true);
            echo ">
              ";
            // line 49
            $this->displayBlock('title', $context, $blocks);
            // line 52
            echo "            </h2>
          ";
        }
        // line 54
        echo "
          <div";
        // line 55
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["author_attributes"] ?? null), "addClass", [0 => ($context["author_classes"] ?? null)], "method", false, false, true, 55), 55, $this->source), "html", null, true);
        echo ">
            ";
        // line 56
        $this->displayBlock('submitted', $context, $blocks);
        // line 60
        echo "          </div>

        ";
    }

    // line 49
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 50
        echo "                <a class=\"";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 50, $this->source), "link"), "html", null, true);
        echo "\" href=\"";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["url"] ?? null), 50, $this->source), "html", null, true);
        echo "\" rel=\"bookmark\">";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 50, $this->source), "html", null, true);
        echo "</a>
              ";
    }

    // line 56
    public function block_submitted($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 57
        echo "              ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["date"] ?? null), 57, $this->source), "html", null, true);
        echo "
              ";
        // line 58
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["metadata"] ?? null), 58, $this->source), "html", null, true);
        echo "
            ";
    }

    // line 67
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 68
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->withoutFilter($this->sandbox->ensureToStringAllowed(($context["content"] ?? null), 68, $this->source), "links"), "html", null, true);
        echo "
      ";
    }

    // line 74
    public function block_links($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 75
        echo "          ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "links", [], "any", false, false, true, 75), 75, $this->source), "html", null, true);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/content/node/node.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 75,  217 => 74,  210 => 68,  206 => 67,  200 => 58,  195 => 57,  191 => 56,  180 => 50,  176 => 49,  170 => 60,  168 => 56,  164 => 55,  161 => 54,  157 => 52,  155 => 49,  150 => 48,  147 => 47,  143 => 46,  138 => 79,  134 => 77,  132 => 74,  127 => 73,  125 => 72,  121 => 70,  119 => 67,  115 => 66,  112 => 65,  108 => 63,  106 => 46,  101 => 45,  98 => 44,  94 => 43,  89 => 81,  87 => 43,  82 => 41,  78 => 40,  73 => 39,  69 => 37,  67 => 36,  65 => 32,  64 => 31,  62 => 27,  61 => 26,  59 => 22,  58 => 21,  56 => 17,  55 => 16,  53 => 12,  52 => 11,  51 => 10,  50 => 9,  48 => 5,  47 => 4,  46 => 3,  45 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{%
  set block = bem_block([
    node.entityTypeId(),
    node.bundle(),
    view_mode != 'default' ? view_mode,
  ])
%}
{%
  set modifiers = [
    node.isPromoted() ? 'promoted',
    node.isSticky() ? 'sticky',
    not node.isPublished() ? 'unpublished',
  ]
%}
{%
  set classes = [
    bem(block, null, modifiers),
  ]
%}
{%
  set title_classes = [
    bem(block, 'title'),
  ]
%}
{%
  set author_classes = [
    bem(block, 'submitted'),
  ]
%}
{%
  set content_classes = [
    bem(block, 'content'),
  ]
%}
{# @see https://www.drupal.org/project/drupal/issues/3117230 #}
{% if attributes.hasAttribute('role') %}
  {% do attributes.removeAttribute('role') %}
{% endif %}
<article{{ attributes.addClass(classes) }}>
  {{ title_prefix }}
  {{ title_suffix }}

  {% block node %}
    {% if display_submitted %}
      <header class=\"{{ bem(block, 'header') }}\">
        {% block header %}
          {% if not page %}
            <h2{{ title_attributes.addClass(title_classes) }}>
              {% block title %}
                <a class=\"{{ bem(block, 'link') }}\" href=\"{{ url }}\" rel=\"bookmark\">{{ label }}</a>
              {% endblock %}
            </h2>
          {% endif %}

          <div{{ author_attributes.addClass(author_classes) }}>
            {% block submitted %}
              {{ date }}
              {{ metadata }}
            {% endblock %}
          </div>

        {% endblock %}
      </header>
    {% endif %}

    <div{{ content_attributes.addClass(content_classes) }}>
      {% block content %}
        {{ content|without('links') }}
      {% endblock %}
    </div>

    {% if content.links %}
      <div class=\"{{ bem(block, 'links') }}\">
        {% block links %}
          {{ content.links }}
        {% endblock %}
      </div>
    {% endif %}

  {% endblock node %}
</article>
", "themes/contrib/glisseo/templates/content/node/node.html.twig", "/app/web/themes/contrib/glisseo/templates/content/node/node.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 2, "if" => 36, "do" => 37, "block" => 43);
        static $filters = array("escape" => 39, "without" => 68);
        static $functions = array("bem_block" => 2, "bem" => 17);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'do', 'block'],
                ['escape', 'without'],
                ['bem_block', 'bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
