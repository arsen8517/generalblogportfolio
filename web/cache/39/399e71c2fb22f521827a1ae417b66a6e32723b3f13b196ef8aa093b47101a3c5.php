<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/region/region--header.html.twig */

class __TwigTemplate_24e14b4acc782f31e19c115caa162f98a8e970f8355bf244f903c26ab0794930 extends \Twig\Template
{
  private $source;
  private $macros = [];

  public function __construct(Environment $env)
  {
    parent::__construct($env);

    $this->source = $this->getSourceContext();

    $this->blocks = [
      'region' => [$this, 'block_region'],
    ];
    $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
    $this->checkSecurity();
  }

  public function block_region($context, array $blocks = [])
  {
    $macros = $this->macros;
    // line 4
    echo "  <header";
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 4), 4, $this->source), "html", null, true);
    echo ">
    <div class=\"layout-container ";
    // line 5
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 5, $this->source), "content"), "html", null, true);
    echo "\">
      ";
    // line 6
    $this->displayBlock("content", $context, $blocks);
    echo "
    </div>
  </header>
";
  }

  public function getTemplateName()
  {
    return "themes/contrib/glisseo/templates/region/region--header.html.twig";
  }

  // line 3

  public function isTraitable()
  {
    return false;
  }

  public function getDebugInfo()
  {
    return array(61 => 6, 57 => 5, 52 => 4, 48 => 3, 37 => 1,);
  }

  public function getSourceContext()
  {
    return new Source("{% extends 'region--header.html.twig' %}

{% block region %}
  <header{{ attributes.addClass(classes) }}>
    <div class=\"layout-container {{ bem(block, 'content') }}\">
      {{ block('content') }}
    </div>
  </header>
{% endblock %}
", "themes/contrib/glisseo/templates/region/region--header.html.twig", "/app/web/themes/contrib/glisseo/templates/region/region--header.html.twig");
  }

  public function checkSecurity()
  {
    static $tags = array();
    static $filters = array("escape" => 4);
    static $functions = array("bem" => 5);

    try {
      $this->sandbox->checkSecurity(
        [],
        ['escape'],
        ['bem']
      );
    } catch (SecurityError $e) {
      $e->setSourceContext($this->source);

      if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
        $e->setTemplateLine($tags[$e->getTagName()]);
      } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
        $e->setTemplateLine($filters[$e->getFilterName()]);
      } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
        $e->setTemplateLine($functions[$e->getFunctionName()]);
      }

      throw $e;
    }

  }

  protected function doGetParent(array $context)
  {
    // line 1
    return "region--header.html.twig";
  }

  protected function doDisplay(array $context, array $blocks = [])
  {
    $macros = $this->macros;
    $this->parent = $this->loadTemplate("region--header.html.twig", "themes/contrib/glisseo/templates/region/region--header.html.twig", 1);
    $this->parent->display($context, array_merge($this->blocks, $blocks));
  }
}
