<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/form/input.html.twig */
class __TwigTemplate_9302341b48f21b747c7a080ad44b69601d77a46c0cda6544026062552583b11e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        $context["block"] = (($context["block"]) ?? ("form-element"));
        // line 20
        $context["modifiers"] = [0 => ((        // line 21
($context["button_type"] ?? null)) ? (($context["button_type"] ?? null)) : ("")), 1 => ("type-" . $this->sandbox->ensureToStringAllowed(        // line 22
($context["element_type"] ?? null), 22, $this->source)), 2 => ("name-" . $this->sandbox->ensureToStringAllowed(        // line 23
($context["element_name"] ?? null), 23, $this->source))];
        // line 26
        if (twig_in_filter(($context["element_type"] ?? null), [0 => "checkbox", 1 => "radio"])) {
            // line 27
            echo "  ";
            $context["block"] = "form-boolean";
        } elseif (twig_in_filter(        // line 28
($context["element_type"] ?? null), [0 => "submit"])) {
            // line 29
            echo "  ";
            $context["block"] = "button";
        }
        // line 32
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 33
($context["block"] ?? null), 33, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["modifiers"] ?? null), 33, $this->source))];
        // line 36
        echo "<input";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 36), 36, $this->source), "html", null, true);
        echo " />";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["children"] ?? null), 36, $this->source), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/form/input.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 36,  58 => 33,  57 => 32,  53 => 29,  51 => 28,  48 => 27,  46 => 26,  44 => 23,  43 => 22,  42 => 21,  41 => 20,  39 => 18,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Theme override for an 'input' #type form element.
 *
 * Available variables:
 * - attributes: A list of HTML attributes for the input element.
 * - children: Optional additional rendered elements.
 * - element_type: The element type name.
 * - element_name: The element name.
 * - button_type: If element is button and button type is set, it will contain
 *   the type name.
 *
 * @see template_preprocess_input()
 * @see glisseo_preprocess_input()
 */
#}
{% set block = block ?? 'form-element' %}
{%
  set modifiers = [
    button_type ? button_type,
    'type-' ~ element_type,
    'name-' ~ element_name,
  ]
%}
{% if element_type in ['checkbox', 'radio'] %}
  {% set block = 'form-boolean' %}
{% elseif element_type in ['submit'] %}
  {% set block = 'button' %}
{% endif %}
{%
  set classes = [
    bem(block, null, modifiers),
  ]
%}
<input{{ attributes.addClass(classes) }} />{{ children }}
", "themes/contrib/glisseo/templates/form/input.html.twig", "/app/web/themes/contrib/glisseo/templates/form/input.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 18, "if" => 26);
        static $filters = array("escape" => 36);
        static $functions = array("bem" => 33);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape'],
                ['bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
