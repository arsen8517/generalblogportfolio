<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/generalblog/templates/layout/region--header.html.twig */
class __TwigTemplate_124eb8273cc8b34943ad80689c2956981464e6dc7768f7b3eac016003b0899f3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["classes"] = [0 => "header", 1 => "region", 2 => ("region-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 4
($context["region"] ?? null), 4, $this->source)))];
        // line 6
        echo "
";
        // line 7
        if (($context["content"] ?? null)) {
            // line 8
            echo "  <header";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 8), 8, $this->source), "html", null, true);
            echo ">
    <div class=\"header-wrap\">
      <div class=\"name-of-branding-site\"><a>ux designer & art direction</a></div>
      <div class=\"nav home\"><a>home</a></div>
      <div class=\"nav my-account\"><a>my account</a></div>
      <div class=\"nav log-out\"><a>log-out</a></div>
    </div>
  </header>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/custom/generalblog/templates/layout/region--header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 8,  45 => 7,  42 => 6,  40 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set classes = [
  'header',
  'region',
  'region-' ~ region|clean_class,
] %}

{% if content %}
  <header{{ attributes.addClass(classes) }}>
    <div class=\"header-wrap\">
      <div class=\"name-of-branding-site\"><a>ux designer & art direction</a></div>
      <div class=\"nav home\"><a>home</a></div>
      <div class=\"nav my-account\"><a>my account</a></div>
      <div class=\"nav log-out\"><a>log-out</a></div>
    </div>
  </header>
{% endif %}
", "themes/custom/generalblog/templates/layout/region--header.html.twig", "/app/web/themes/custom/generalblog/templates/layout/region--header.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 7);
        static $filters = array("clean_class" => 4, "escape" => 8);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
