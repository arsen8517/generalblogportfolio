<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/generalblog/templates/secondPage/layout/region--header.html.twig */
class __TwigTemplate_49f8a38683c3653c300d604bf759ec17249067851d2897cfd800d5ff6ec8418b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'header' => [$this, 'block_header'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["classes"] = [0 => "header"];
        // line 4
        echo "
 ";
        // line 5
        $this->displayBlock('header', $context, $blocks);
        // line 15
        echo "
";
    }

    // line 5
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "   <div class=\"header\">
     <div class=\"header-wrap\">
       <div class=\"name-of-branding-site\"><a>ux designer & art direction</a></div>
       <div class=\"nav home\"><a>home</a></div>
       <div class=\"nav my-account\"><a>my account</a></div>
       <div class=\"nav log-out\"><a>log-out</a></div>
     </div>
   </div>
 ";
    }

    public function getTemplateName()
    {
        return "themes/custom/generalblog/templates/secondPage/layout/region--header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 6,  52 => 5,  47 => 15,  45 => 5,  42 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set classes = [
  'header'
] %}

 {% block header %}
   <div class=\"header\">
     <div class=\"header-wrap\">
       <div class=\"name-of-branding-site\"><a>ux designer & art direction</a></div>
       <div class=\"nav home\"><a>home</a></div>
       <div class=\"nav my-account\"><a>my account</a></div>
       <div class=\"nav log-out\"><a>log-out</a></div>
     </div>
   </div>
 {% endblock %}

", "themes/custom/generalblog/templates/secondPage/layout/region--header.html.twig", "/app/web/themes/custom/generalblog/templates/secondPage/layout/region--header.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "block" => 5);
        static $filters = array();
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block'],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
