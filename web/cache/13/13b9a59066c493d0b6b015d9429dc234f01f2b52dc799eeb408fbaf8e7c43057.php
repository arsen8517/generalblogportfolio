<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/form/form-element-label.html.twig */
class __TwigTemplate_3e95f4c45ef6cf6a343f23a157fa2479c607eed93a072109bc4dff21dbe44e80 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        $context["block"] = (($context["block"]) ?? ("form-item"));
        // line 16
        $context["element"] = "label";
        // line 18
        $context["modifiers"] = [0 => ((        // line 19
($context["required"] ?? null)) ? ("required") : ("")), 1 => (((        // line 20
($context["title_display"] ?? null) == "after")) ? ("inline") : (""))];
        // line 24
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 25
($context["block"] ?? null), 25, $this->source), $this->sandbox->ensureToStringAllowed(($context["element"] ?? null), 25, $this->source), $this->sandbox->ensureToStringAllowed(($context["modifiers"] ?? null), 25, $this->source)), 1 => ((        // line 26
($context["required"] ?? null)) ? ("js-form-required") : ("")), 2 => ((        // line 27
($context["required"] ?? null)) ? ("form-required") : ("")), 3 => (((        // line 28
($context["title_display"] ?? null) == "invisible")) ? ("visually-hidden") : (""))];
        // line 31
        if (( !twig_test_empty(($context["title"] ?? null)) || ($context["required"] ?? null))) {
            // line 32
            echo "<label";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 32), 32, $this->source), "html", null, true);
            echo ">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null), 32, $this->source), "html", null, true);
            echo "</label>";
        }
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/form/form-element-label.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 32,  53 => 31,  51 => 28,  50 => 27,  49 => 26,  48 => 25,  47 => 24,  45 => 20,  44 => 19,  43 => 18,  41 => 16,  39 => 15,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Template override for a form element label.
 *
 * Available variables:
 * - title: The label's text.
 * - title_display: Elements title_display setting.
 * - required: An indicator for whether the associated form element is required.
 * - attributes: A list of HTML attributes for the label.
 *
 * @see template_preprocess_form_element_label()
 */
#}
{% set block = block ?? 'form-item' %}
{% set element = 'label' %}
{%
  set modifiers = [
    required ? 'required',
    title_display == 'after' ? 'inline',
  ]
%}
{%
  set classes = [
    bem(block, element, modifiers),
    required ? 'js-form-required',
    required ? 'form-required',
    title_display == 'invisible' ? 'visually-hidden',
  ]
%}
{% if title is not empty or required -%}
  <label{{ attributes.addClass(classes) }}>{{ title }}</label>
{%- endif %}
", "themes/contrib/glisseo/templates/form/form-element-label.html.twig", "/app/web/themes/contrib/glisseo/templates/form/form-element-label.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 15, "if" => 31);
        static $filters = array("escape" => 32);
        static $functions = array("bem" => 25);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape'],
                ['bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
