<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/views/views-view.html.twig */
class __TwigTemplate_50782f74b9eb39c2b7187193080319e9f35ef95e6da75bfd8d9f56f8df05e6c9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'header' => [$this, 'block_header'],
            'filters' => [$this, 'block_filters'],
            'attachment_before' => [$this, 'block_attachment_before'],
            'content' => [$this, 'block_content'],
            'content_empty' => [$this, 'block_content_empty'],
            'pager' => [$this, 'block_pager'],
            'attachment_after' => [$this, 'block_attachment_after'],
            'more' => [$this, 'block_more'],
            'footer' => [$this, 'block_footer'],
            'feed' => [$this, 'block_feed'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["block"] = ((($context["css_name"] ?? null)) ? (($context["css_name"] ?? null)) : ("views"));
        // line 3
        $context["modifiers"] = [0 => (((        // line 4
($context["block"] ?? null) == "views")) ? (($context["id"] ?? null)) : ("")), 1 =>         // line 5
($context["display_id"] ?? null)];
        // line 9
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 10
($context["block"] ?? null), 10, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["modifiers"] ?? null), 10, $this->source)), 1 => ((        // line 11
($context["dom_id"] ?? null)) ? (("js-view-dom-id-" . $this->sandbox->ensureToStringAllowed(($context["dom_id"] ?? null), 11, $this->source))) : (""))];
        // line 14
        echo "<div";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 14), 14, $this->source), "html", null, true);
        echo ">
  ";
        // line 15
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null), 15, $this->source), "html", null, true);
        echo "

  ";
        // line 17
        if (($context["title"] ?? null)) {
            // line 18
            echo "    ";
            $this->displayBlock('title', $context, $blocks);
            // line 21
            echo "  ";
        }
        // line 22
        echo "
  ";
        // line 23
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null), 23, $this->source), "html", null, true);
        echo "

  ";
        // line 25
        if (($context["header"] ?? null)) {
            // line 26
            echo "    <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 26, $this->source), "header"), "html", null, true);
            echo "\">
      ";
            // line 27
            $this->displayBlock('header', $context, $blocks);
            // line 30
            echo "    </div>
  ";
        }
        // line 32
        echo "
  ";
        // line 33
        if (($context["exposed"] ?? null)) {
            // line 34
            echo "    <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 34, $this->source), "filters"), "html", null, true);
            echo "\">
      ";
            // line 35
            $this->displayBlock('filters', $context, $blocks);
            // line 38
            echo "    </div>
  ";
        }
        // line 40
        echo "
  ";
        // line 41
        if (($context["attachment_before"] ?? null)) {
            // line 42
            echo "    <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 42, $this->source), "attachment", [0 => "before"]), "html", null, true);
            echo "\">
      ";
            // line 43
            $this->displayBlock('attachment_before', $context, $blocks);
            // line 46
            echo "    </div>
  ";
        }
        // line 48
        echo "
  ";
        // line 49
        if (($context["rows"] ?? null)) {
            // line 50
            echo "    <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 50, $this->source), "content"), "html", null, true);
            echo "\">
      ";
            // line 51
            $this->displayBlock('content', $context, $blocks);
            // line 54
            echo "    </div>
  ";
        } elseif (        // line 55
($context["empty"] ?? null)) {
            // line 56
            echo "    <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 56, $this->source), "content", [0 => "empty"]), "html", null, true);
            echo "\">
      ";
            // line 57
            $this->displayBlock('content_empty', $context, $blocks);
            // line 60
            echo "    </div>
  ";
        }
        // line 62
        echo "
  ";
        // line 63
        if (($context["pager"] ?? null)) {
            // line 64
            echo "    <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 64, $this->source), "pager"), "html", null, true);
            echo "\">
      ";
            // line 65
            $this->displayBlock('pager', $context, $blocks);
            // line 68
            echo "    </div>
  ";
        }
        // line 70
        echo "
  ";
        // line 71
        if (($context["attachment_after"] ?? null)) {
            // line 72
            echo "    <div class=";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 72, $this->source), "attachment", [0 => "after"]), "html", null, true);
            echo ">
      ";
            // line 73
            $this->displayBlock('attachment_after', $context, $blocks);
            // line 76
            echo "    </div>
  ";
        }
        // line 78
        echo "
  ";
        // line 79
        if (($context["more"] ?? null)) {
            // line 80
            echo "    ";
            $this->displayBlock('more', $context, $blocks);
            // line 83
            echo "  ";
        }
        // line 84
        echo "
  ";
        // line 85
        if (($context["footer"] ?? null)) {
            // line 86
            echo "    <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 86, $this->source), "footer"), "html", null, true);
            echo "\">
      ";
            // line 87
            $this->displayBlock('footer', $context, $blocks);
            // line 90
            echo "    </div>
  ";
        }
        // line 92
        echo "
  ";
        // line 93
        if (($context["feed_icons"] ?? null)) {
            // line 94
            echo "    <div class=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, _glisseo_bem($this->sandbox->ensureToStringAllowed(($context["block"] ?? null), 94, $this->source), "feed-icons"), "html", null, true);
            echo "\">
      ";
            // line 95
            $this->displayBlock('feed', $context, $blocks);
            // line 98
            echo "    </div>
  ";
        }
        // line 100
        echo "</div>
";
    }

    // line 18
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "      ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null), 19, $this->source), "html", null, true);
        echo "
    ";
    }

    // line 27
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header"] ?? null), 28, $this->source), "html", null, true);
        echo "
      ";
    }

    // line 35
    public function block_filters($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["exposed"] ?? null), 36, $this->source), "html", null, true);
        echo "
      ";
    }

    // line 43
    public function block_attachment_before($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attachment_before"] ?? null), 44, $this->source), "html", null, true);
        echo "
      ";
    }

    // line 51
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["rows"] ?? null), 52, $this->source), "html", null, true);
        echo "
      ";
    }

    // line 57
    public function block_content_empty($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["empty"] ?? null), 58, $this->source), "html", null, true);
        echo "
      ";
    }

    // line 65
    public function block_pager($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 66
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pager"] ?? null), 66, $this->source), "html", null, true);
        echo "
      ";
    }

    // line 73
    public function block_attachment_after($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 74
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attachment_after"] ?? null), 74, $this->source), "html", null, true);
        echo "
      ";
    }

    // line 80
    public function block_more($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 81
        echo "      ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["more"] ?? null), 81, $this->source), "html", null, true);
        echo "
    ";
    }

    // line 87
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 88
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer"] ?? null), 88, $this->source), "html", null, true);
        echo "
      ";
    }

    // line 95
    public function block_feed($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 96
        echo "        ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["feed_icons"] ?? null), 96, $this->source), "html", null, true);
        echo "
      ";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/views/views-view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  353 => 96,  349 => 95,  342 => 88,  338 => 87,  331 => 81,  327 => 80,  320 => 74,  316 => 73,  309 => 66,  305 => 65,  298 => 58,  294 => 57,  287 => 52,  283 => 51,  276 => 44,  272 => 43,  265 => 36,  261 => 35,  254 => 28,  250 => 27,  243 => 19,  239 => 18,  234 => 100,  230 => 98,  228 => 95,  223 => 94,  221 => 93,  218 => 92,  214 => 90,  212 => 87,  207 => 86,  205 => 85,  202 => 84,  199 => 83,  196 => 80,  194 => 79,  191 => 78,  187 => 76,  185 => 73,  180 => 72,  178 => 71,  175 => 70,  171 => 68,  169 => 65,  164 => 64,  162 => 63,  159 => 62,  155 => 60,  153 => 57,  148 => 56,  146 => 55,  143 => 54,  141 => 51,  136 => 50,  134 => 49,  131 => 48,  127 => 46,  125 => 43,  120 => 42,  118 => 41,  115 => 40,  111 => 38,  109 => 35,  104 => 34,  102 => 33,  99 => 32,  95 => 30,  93 => 27,  88 => 26,  86 => 25,  81 => 23,  78 => 22,  75 => 21,  72 => 18,  70 => 17,  65 => 15,  60 => 14,  58 => 11,  57 => 10,  56 => 9,  54 => 5,  53 => 4,  52 => 3,  50 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set block = css_name ? css_name : 'views' %}
{%
  set modifiers = [
    block == 'views' ? id,
    display_id,
  ]
%}
{%
  set classes = [
    bem(block, null, modifiers),
    dom_id ? 'js-view-dom-id-' ~ dom_id,
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {{ title_prefix }}

  {% if title %}
    {% block title %}
      {{ title }}
    {% endblock %}
  {% endif %}

  {{ title_suffix }}

  {% if header %}
    <div class=\"{{ bem(block, 'header') }}\">
      {% block header %}
        {{ header }}
      {% endblock %}
    </div>
  {% endif %}

  {% if exposed %}
    <div class=\"{{ bem(block, 'filters') }}\">
      {% block filters %}
        {{ exposed }}
      {% endblock %}
    </div>
  {% endif %}

  {% if attachment_before %}
    <div class=\"{{ bem(block, 'attachment', ['before']) }}\">
      {% block attachment_before %}
        {{ attachment_before }}
      {% endblock attachment_before %}
    </div>
  {% endif %}

  {% if rows %}
    <div class=\"{{ bem(block, 'content') }}\">
      {% block content %}
        {{ rows }}
      {% endblock %}
    </div>
  {% elseif empty %}
    <div class=\"{{ bem(block, 'content', ['empty']) }}\">
      {% block content_empty %}
        {{ empty }}
      {% endblock %}
    </div>
  {% endif %}

  {% if pager %}
    <div class=\"{{ bem(block, 'pager') }}\">
      {% block pager %}
        {{ pager }}
      {% endblock %}
    </div>
  {% endif %}

  {% if attachment_after %}
    <div class={{ bem(block, 'attachment', ['after']) }}>
      {% block attachment_after %}
        {{ attachment_after }}
      {% endblock %}
    </div>
  {% endif %}

  {% if more %}
    {% block more %}
      {{ more }}
    {% endblock %}
  {% endif %}

  {% if footer %}
    <div class=\"{{ bem(block, 'footer') }}\">
      {% block footer %}
        {{ footer }}
      {% endblock %}
    </div>
  {% endif %}

  {% if feed_icons %}
    <div class=\"{{ bem(block, 'feed-icons') }}\">
      {% block feed %}
        {{ feed_icons }}
      {% endblock %}
    </div>
  {% endif %}
</div>
", "themes/contrib/glisseo/templates/views/views-view.html.twig", "/app/web/themes/contrib/glisseo/templates/views/views-view.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 17, "block" => 18);
        static $filters = array("escape" => 14);
        static $functions = array("bem" => 10);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['escape'],
                ['bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
