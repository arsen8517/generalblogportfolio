<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/field/field--header.html.twig */
class __TwigTemplate_cc038c526ffbde5d9e84b1732cf33d3e8639f937ceda94c623c886c065111a67 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'field' => [$this, 'block_field'],
            'label' => [$this, 'block_label'],
            'items' => [$this, 'block_items'],
            'item' => [$this, 'block_item'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        $context["field_block"] = "field";
        // line 18
        $context["field_modifiers"] = [0 => ("name-" . $this->sandbox->ensureToStringAllowed(        // line 19
($context["field_name"] ?? null), 19, $this->source)), 1 => ("type-" . $this->sandbox->ensureToStringAllowed(        // line 20
($context["field_type"] ?? null), 20, $this->source)), 2 => ("label-" . $this->sandbox->ensureToStringAllowed(        // line 21
($context["label_display"] ?? null), 21, $this->source))];
        // line 25
        $context["entity_block"] = _glisseo_bem_block([0 =>         // line 26
($context["entity_type"] ?? null), 1 => (((        // line 27
($context["bundle"] ?? null) != ($context["entity_type"] ?? null))) ? (($context["bundle"] ?? null)) : ("")), 2 =>         // line 28
($context["field_name"] ?? null)]);
        // line 32
        $context["entity_modifiers"] = [0 =>         // line 33
($context["view_mode"] ?? null)];
        // line 37
        $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 38
($context["entity_block"] ?? null), 38, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["entity_modifiers"] ?? null), 38, $this->source)), 1 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 39
($context["field_block"] ?? null), 39, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["field_modifiers"] ?? null), 39, $this->source))];
        // line 43
        $context["title_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 44
($context["entity_block"] ?? null), 44, $this->source), "label"), 1 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 45
($context["field_block"] ?? null), 45, $this->source), "label")];
        // line 48
        $context["content_element"] = ((($context["multiple"] ?? null)) ? ("items") : ("value"));
        // line 50
        $context["content_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 51
($context["entity_block"] ?? null), 51, $this->source), $this->sandbox->ensureToStringAllowed(($context["content_element"] ?? null), 51, $this->source)), 1 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 52
($context["field_block"] ?? null), 52, $this->source), $this->sandbox->ensureToStringAllowed(($context["content_element"] ?? null), 52, $this->source))];
        // line 56
        $context["item_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 57
($context["entity_block"] ?? null), 57, $this->source), "item"), 1 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 58
($context["field_block"] ?? null), 58, $this->source), "item")];
        // line 61
        echo "<div";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 61), 61, $this->source), "html", null, true);
        echo ">
  ";
        // line 62
        $this->displayBlock('field', $context, $blocks);
        // line 93
        echo "</div>
";
    }

    // line 62
    public function block_field($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        echo "
    ";
        // line 64
        if ( !($context["label_hidden"] ?? null)) {
            // line 65
            echo "      <div";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["title_attributes"] ?? null), "addClass", [0 => ($context["title_classes"] ?? null)], "method", false, false, true, 65), 65, $this->source), "html", null, true);
            echo ">
        ";
            // line 66
            $this->displayBlock('label', $context, $blocks);
            // line 69
            echo "      </div>
    ";
        }
        // line 71
        echo "
    ";
        // line 72
        $this->displayBlock('items', $context, $blocks);
        // line 91
        echo "
  ";
    }

    // line 66
    public function block_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 67
        echo "          ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 67, $this->source), "html", null, true);
        echo "
        ";
    }

    // line 72
    public function block_items($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 73
        echo "      ";
        ob_start();
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content_attributes"] ?? null), 73, $this->source), "html", null, true);
        $context["content_attributes_not_empty"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 74
        echo "      <div";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method", false, false, true, 74), 74, $this->source), "html", null, true);
        echo ">
        ";
        // line 75
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 76
            echo "          ";
            ob_start();
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "attributes", [], "any", false, false, true, 76), 76, $this->source), "html", null, true);
            $context["item_attributes_not_empty"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 77
            echo "          ";
            if ((($context["multiple"] ?? null) || ($context["item_attributes_not_empty"] ?? null))) {
                // line 78
                echo "            <div";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "attributes", [], "any", false, false, true, 78), "addClass", [0 => ($context["item_classes"] ?? null)], "method", false, false, true, 78), 78, $this->source), "html", null, true);
                echo ">
          ";
            }
            // line 81
            $this->displayBlock('item', $context, $blocks);
            // line 84
            echo "
          ";
            // line 85
            if ((($context["multiple"] ?? null) || ($context["item_attributes_not_empty"] ?? null))) {
                // line 86
                echo "            </div> ";
                // line 87
                echo "          ";
            }
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "      </div> ";
        // line 90
        echo "    ";
    }

    // line 81
    public function block_item($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 82
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "content", [], "any", false, false, true, 82), 82, $this->source), "html", null, true);
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/field/field--header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 82,  204 => 81,  200 => 90,  198 => 89,  183 => 87,  181 => 86,  179 => 85,  176 => 84,  174 => 81,  168 => 78,  165 => 77,  160 => 76,  143 => 75,  138 => 74,  133 => 73,  129 => 72,  122 => 67,  118 => 66,  113 => 91,  111 => 72,  108 => 71,  104 => 69,  102 => 66,  97 => 65,  95 => 64,  92 => 63,  88 => 62,  83 => 93,  81 => 62,  76 => 61,  74 => 58,  73 => 57,  72 => 56,  70 => 52,  69 => 51,  68 => 50,  66 => 48,  64 => 45,  63 => 44,  62 => 43,  60 => 39,  59 => 38,  58 => 37,  56 => 33,  55 => 32,  53 => 28,  52 => 27,  51 => 26,  50 => 25,  48 => 21,  47 => 20,  46 => 19,  45 => 18,  43 => 16,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Default theme implementation to present field.
 *
 * Available variables:
 * - attributes: HTML attributes for containing element.
 * - classes: An array of classes for main wrapper.
 * - entity_type: The name of entity type, which field belongs to.
 * - bundle: The entity bundle name for the field.
 * - view_mode: The view mode used to render field.
 *
 * @see glisseo_preprocess_field()
 */
#}
{% set field_block = 'field' %}
{%
  set field_modifiers = [
    'name-' ~ field_name,
    'type-' ~ field_type,
    'label-' ~ label_display,
  ]
%}
{%
  set entity_block = bem_block([
    entity_type,
    bundle != entity_type ? bundle,
    field_name,
  ])
%}
{%
  set entity_modifiers = [
    view_mode,
  ]
%}
{%
  set classes = [
    bem(entity_block, null, entity_modifiers),
    bem(field_block, null, field_modifiers),
  ]
%}
{%
  set title_classes = [
    bem(entity_block, 'label'),
    bem(field_block, 'label'),
  ]
%}
{% set content_element = multiple ? 'items' : 'value' %}
{%
  set content_classes = [
    bem(entity_block, content_element),
    bem(field_block, content_element),
  ]
%}
{%
  set item_classes = [
    bem(entity_block, 'item'),
    bem(field_block, 'item'),
  ]
%}
<div{{ attributes.addClass(classes) }}>
  {% block field %}

    {% if not label_hidden %}
      <div{{ title_attributes.addClass(title_classes) }}>
        {% block label %}
          {{ label }}
        {% endblock %}
      </div>
    {% endif %}

    {% block items %}
      {% set content_attributes_not_empty -%}{{ content_attributes }}{%- endset %}
      <div{{ content_attributes.addClass(content_classes) }}>
        {% for item in items %}
          {% set item_attributes_not_empty -%}{{ item.attributes }}{%- endset %}
          {% if multiple or item_attributes_not_empty %}
            <div{{ item.attributes.addClass(item_classes) }}>
          {% endif -%}

          {% block item %}
            {{- item.content -}}
          {% endblock %}

          {% if multiple or item_attributes_not_empty %}
            </div> {# /__item #}
          {% endif -%}
        {% endfor %}
      </div> {# /__items and __value #}
    {% endblock %}

  {% endblock %}
</div>
", "themes/contrib/glisseo/templates/field/field--header.html.twig", "/app/web/themes/contrib/glisseo/templates/field/field--header.html.twig");
    }

    public function checkSecurity()
    {
        static $tags = array("set" => 16, "block" => 62, "if" => 64, "for" => 75);
        static $filters = array("escape" => 61);
        static $functions = array("bem_block" => 25, "bem" => 38);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if', 'for'],
                ['escape'],
                ['bem_block', 'bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
