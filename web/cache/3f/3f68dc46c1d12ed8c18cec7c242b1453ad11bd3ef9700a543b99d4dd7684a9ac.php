<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/generalblog/templates/layout/region--header.html.twig */

class __TwigTemplate_bf0d700b3d27f1a0e1f0c646c328656dffd3d3a7840ed17825827be40b645a0c extends \Twig\Template
{
  private $source;
  private $macros = [];

  public function __construct(Environment $env)
  {
    parent::__construct($env);

    $this->source = $this->getSourceContext();

    $this->parent = false;

    $this->blocks = [
    ];
    $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
    $this->checkSecurity();
  }

  public function getTemplateName()
  {
    return "themes/custom/generalblog/templates/layout/region--header.html.twig";
  }

  public function isTraitable()
  {
    return false;
  }

  public function getDebugInfo()
  {
    return array(53 => 24, 47 => 22, 45 => 21, 43 => 19, 42 => 16, 39 => 15,);
  }

  public function getSourceContext()
  {
    return new Source("{#
/**
 * @file
 * Theme override to display a region.
 *
 * Available variables:
 * - content: The content for this region, typically blocks.
 * - attributes: HTML attributes for the region <div>.
 * - region: The name of the region variable as defined in the theme's
 *   .info.yml file.
 *
 * @see template_preprocess_region()
 */
#}

{% set classes = [
  'header',
  'region',
  'region-' ~ region|clean_class,
] %}
{% if content %}
  <header{{ attributes.addClass(classes) }}>
    <div class=\"header-wrap\">
      {{ content }}
    </div>
  </header>
{% endif %}
", "themes/custom/generalblog/templates/layout/region--header.html.twig", "/app/web/themes/custom/generalblog/templates/layout/region--header.html.twig");
  }

  public function checkSecurity()
  {
    static $tags = array("set" => 16, "if" => 21);
    static $filters = array("clean_class" => 19, "escape" => 22);
    static $functions = array();

    try {
      $this->sandbox->checkSecurity(
        ['set', 'if'],
        ['clean_class', 'escape'],
        []
      );
    } catch (SecurityError $e) {
      $e->setSourceContext($this->source);

      if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
        $e->setTemplateLine($tags[$e->getTagName()]);
      } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
        $e->setTemplateLine($filters[$e->getFilterName()]);
      } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
        $e->setTemplateLine($functions[$e->getFunctionName()]);
      }

      throw $e;
    }

  }

  protected function doDisplay(array $context, array $blocks = [])
  {
    $macros = $this->macros;
    // line 15
    echo "
";
    // line 16
    $context["classes"] = [0 => "header", 1 => "region", 2 => ("region-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 19
        ($context["region"] ?? null), 19, $this->source)))];
    // line 21
    if (($context["content"] ?? null)) {
      // line 22
      echo "  <header";
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 22), 22, $this->source), "html", null, true);
      echo ">
    <div class=\"header-wrap\">
      ";
      // line 24
      echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null), 24, $this->source), "html", null, true);
      echo "
    </div>
  </header>
";
    }
  }
}
