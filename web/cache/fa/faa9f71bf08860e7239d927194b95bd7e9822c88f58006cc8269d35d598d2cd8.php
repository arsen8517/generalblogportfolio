<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* region--header.html.twig */

class __TwigTemplate_635bdc6a89447e3091ad5f8350a9502630ac48d7214767302d2099106e541216 extends \Twig\Template
{
  private $source;
  private $macros = [];

  public function __construct(Environment $env)
  {
    parent::__construct($env);

    $this->source = $this->getSourceContext();

    $this->parent = false;

    $this->blocks = [
      'region' => [$this, 'block_region'],
      'content' => [$this, 'block_content'],
    ];
    $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
    $this->checkSecurity();
  }

  public function block_region($context, array $blocks = [])
  {
    $macros = $this->macros;
    // line 9
    echo "    <div";
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 9), 9, $this->source), "html", null, true);
    echo ">
      ";
    // line 10
    $this->displayBlock('content', $context, $blocks);
    // line 13
    echo "    </div>
  ";
  }

  public function block_content($context, array $blocks = [])
  {
    $macros = $this->macros;
    // line 11
    echo "        ";
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null), 11, $this->source), "html", null, true);
    echo "
      ";
  }

  // line 10

  public function getTemplateName()
  {
    return "region--header.html.twig";
  }

  public function isTraitable()
  {
    return false;
  }

  public function getDebugInfo()
  {
    return array(73 => 11, 69 => 10, 64 => 13, 62 => 10, 57 => 9, 48 => 8, 46 => 7, 44 => 4, 43 => 3, 41 => 1,);
  }

  public function getSourceContext()
  {
    return new Source("{% set block = block ?? 'region-' ~ region %}
{%
  set classes = [
    bem(block),
  ]
%}
{% if content %}
  {% block region %}
    <div{{ attributes.addClass(classes) }}>
      {% block content %}
        {{ content }}
      {% endblock %}
    </div>
  {% endblock %}
{% endif %}
", "region--header.html.twig", "themes/contrib/glisseo/templates/region/region--header.html.twig");
  }

  public function checkSecurity()
  {
    static $tags = array("set" => 1, "if" => 7, "block" => 8);
    static $filters = array("escape" => 9);
    static $functions = array("bem" => 4);

    try {
      $this->sandbox->checkSecurity(
        ['set', 'if', 'block'],
        ['escape'],
        ['bem']
      );
    } catch (SecurityError $e) {
      $e->setSourceContext($this->source);

      if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
        $e->setTemplateLine($tags[$e->getTagName()]);
      } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
        $e->setTemplateLine($filters[$e->getFilterName()]);
      } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
        $e->setTemplateLine($functions[$e->getFunctionName()]);
      }

      throw $e;
    }

  }

  protected function doDisplay(array $context, array $blocks = [])
  {
    $macros = $this->macros;
    // line 1
    $context["block"] = ((($context["block"]) ?? ("region-")) . $this->sandbox->ensureToStringAllowed(($context["region"] ?? null), 1, $this->source));
    // line 3
    $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(        // line 4
      ($context["block"] ?? null), 4, $this->source))];
    // line 7
    if (($context["content"] ?? null)) {
      // line 8
      echo "  ";
      $this->displayBlock('region', $context, $blocks);
    }
  }
}
