<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/form/field-multiple-value-form.html.twig */
class __TwigTemplate_b349fc5b83e127f0e9202adb0311918ec1eab2a5ab6e0a2095eb02e6f4974e28 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        $context["block"] = (($context["block"]) ?? ("form-item"));
        // line 26
        $context["modifiers"] = [0 => "multiple", 1 => ((        // line 28
($context["disabled"] ?? null)) ? ("disabled") : (""))];
        // line 31
        if (($context["multiple"] ?? null)) {
            // line 32
            echo "  ";
            // line 33
            $context["classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(            // line 34
($context["block"] ?? null), 34, $this->source), null, $this->sandbox->ensureToStringAllowed(($context["modifiers"] ?? null), 34, $this->source)), 1 => "js-form-item"];
            // line 38
            echo "  ";
            // line 39
            $context["description_classes"] = [0 => _glisseo_bem($this->sandbox->ensureToStringAllowed(            // line 40
($context["block"] ?? null), 40, $this->source), "description", [0 => ((($context["disabled"] ?? null)) ? ("is-disabled") : (""))])];
            // line 43
            echo "  <div";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 43), 43, $this->source), "html", null, true);
            echo ">
    ";
            // line 44
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["table"] ?? null), 44, $this->source), "html", null, true);
            echo "
    ";
            // line 45
            if (twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "content", [], "any", false, false, true, 45)) {
                // line 46
                echo "      <div";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "attributes", [], "any", false, false, true, 46), "addClass", [0 => ($context["description_classes"] ?? null)], "method", false, false, true, 46), 46, $this->source), "html", null, true);
                echo " >";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["description"] ?? null), "content", [], "any", false, false, true, 46), 46, $this->source), "html", null, true);
                echo "</div>
    ";
            }
            // line 48
            echo "    ";
            if (($context["button"] ?? null)) {
                // line 49
                echo "      <div class=\"form-actions\">";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["button"] ?? null), 49, $this->source), "html", null, true);
                echo "</div>
    ";
            }
            // line 51
            echo "  </div>
";
        } else {
            // line 53
            echo "  ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["elements"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 54
                echo "    ";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["element"], 54, $this->source), "html", null, true);
                echo "
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/form/field-multiple-value-form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 54,  88 => 53,  84 => 51,  78 => 49,  75 => 48,  67 => 46,  65 => 45,  61 => 44,  56 => 43,  54 => 40,  53 => 39,  51 => 38,  49 => 34,  48 => 33,  46 => 32,  44 => 31,  42 => 28,  41 => 26,  39 => 24,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Theme override for multiple value form element.
 *
 * Available variables for all fields:
 * - multiple: Whether there are multiple instances of the field.
 * - disabled: Whether the inpur is disabled.
 *
 * Available variables for single cardinality fields:
 * - elements: Form elements to be rendered.
 *
 * Available variables when there are multiple fields.
 * - table: Table of field items.
 * - description: The description element containing the following properties:
 *   - content: The description content of the form element.
 *   - attributes: HTML attributes to apply to the description container.
 * - button: \"Add another item\" button.
 *
 * @see template_preprocess_field_multiple_value_form()
 * @see claro_preprocess_field_multiple_value_form()
 */
#}
{% set block = block ?? 'form-item' %}
{%
  set modifiers = [
    'multiple',
    disabled ? 'disabled',
  ]
%}
{% if multiple %}
  {%
    set classes = [
      bem(block, null, modifiers),
      'js-form-item',
    ]
  %}
  {%
    set description_classes = [
      bem(block, 'description', [disabled ? 'is-disabled']),
    ]
  %}
  <div{{ attributes.addClass(classes) }}>
    {{ table }}
    {% if description.content %}
      <div{{ description.attributes.addClass(description_classes) }} >{{ description.content }}</div>
    {% endif %}
    {% if button %}
      <div class=\"form-actions\">{{ button }}</div>
    {% endif %}
  </div>
{% else %}
  {% for element in elements %}
    {{ element }}
  {% endfor %}
{% endif %}
", "themes/contrib/glisseo/templates/form/field-multiple-value-form.html.twig", "/app/web/themes/contrib/glisseo/templates/form/field-multiple-value-form.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 24, "if" => 31, "for" => 53);
        static $filters = array("escape" => 43);
        static $functions = array("bem" => 34);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['escape'],
                ['bem']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
