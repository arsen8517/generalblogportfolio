<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/generalblog/templates/field/field--field-background-image.html.twig */
class __TwigTemplate_66a42f1beaaa01638808029f0f0c24f8d8f03d1f11ceb5d1d1a365c0d4546e3b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->attachLibrary("generalblog/base-page"), "html", null, true);
        echo "

";
        // line 3
        $context["classes"] = [0 => "field", 1 => ("field--name-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 5
($context["field_name"] ?? null), 5, $this->source))), 2 => ("field--type-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(        // line 6
($context["field_type"] ?? null), 6, $this->source))), 3 => ("field--label-" . $this->sandbox->ensureToStringAllowed(        // line 7
($context["label_display"] ?? null), 7, $this->source)), 4 => (((        // line 8
($context["label_display"] ?? null) == "inline")) ? ("clearfix") : ("")), 5 => "background__image__item"];
        // line 11
        echo "<div class=\"description-down-background-image-block\">
  <div";
        // line 12
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 12), 12, $this->source), "html", null, true);
        echo ">
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 14
            echo "      <div";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "attributes", [], "any", false, false, true, 14), "addClass", [0 => "background__image__item"], "method", false, false, true, 14), 14, $this->source), "html", null, true);
            echo ">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "content", [], "any", false, false, true, 14), 14, $this->source), "html", null, true);
            echo "
        <div class=\"description-down-background-tittle\">tittle</div>
      </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/generalblog/templates/field/field--field-background-image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 18,  61 => 14,  57 => 13,  53 => 12,  50 => 11,  48 => 8,  47 => 7,  46 => 6,  45 => 5,  44 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ attach_library('generalblog/base-page') }}

{% set classes = [
  'field',
  'field--name-' ~ field_name|clean_class,
  'field--type-' ~ field_type|clean_class,
  'field--label-' ~ label_display,
  label_display == 'inline' ? 'clearfix',
  'background__image__item',
] %}
<div class=\"description-down-background-image-block\">
  <div{{ attributes.addClass(classes) }}>
    {% for item in items %}
      <div{{ item.attributes.addClass('background__image__item') }}>{{ item.content }}
        <div class=\"description-down-background-tittle\">tittle</div>
      </div>
    {% endfor %}
  </div>
</div>
", "themes/custom/generalblog/templates/field/field--field-background-image.html.twig", "/app/web/themes/custom/generalblog/templates/field/field--field-background-image.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 3, "for" => 13);
        static $filters = array("escape" => 1, "clean_class" => 5);
        static $functions = array("attach_library" => 1);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'for'],
                ['escape', 'clean_class'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
