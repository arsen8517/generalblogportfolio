<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/university/templates/university-making-age.html.twig */

class __TwigTemplate_851e2db7a59f339b1fcb2a666353ac9c22def63cac7454f5281fc72a75a2f2e7 extends \Twig\Template
{
  private $source;
  private $macros = [];

  public function __construct(Environment $env)
  {
    parent::__construct($env);

    $this->source = $this->getSourceContext();

    $this->parent = false;

    $this->blocks = [
    ];
    $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
    $this->checkSecurity();
  }

  public function getTemplateName()
  {
    return "modules/custom/university/templates/university-making-age.html.twig";
  }

  public function isTraitable()
  {
    return false;
  }

  public function getDebugInfo()
  {
    return array(218 => 184, 215 => 182, 212 => 180, 39 => 1,);
  }

  public function getSourceContext()
  {
    return new Source("{{ attach_library('generalblog/base-page') }}
<!DOCTYPE html>
<html lang=\"en\">
<head class=\"main-class-head\">
  <title>The test page in base loc.</title>
</head>
<body>
<div class=\"main-content-body\">
  <div class=\"left-block\">
    <div class=\"left-content-up\">
      <ul class=\"left-content-up-list-up\">
        <li>about me</li>
      </ul>
    </div>
    <div class=\"left-content-center\">
      <ul class=\"left-content-center-list-up\">
        <li>scroll down</li>
      </ul>
    </div>
    <div class=\"left-content-down\">
      <ul class=\"left-content-down-list-up\">
        <li>ideas</li>
      </ul>
    </div>
  </div>
  <div class=\"center-block\">
    <div class=\"center-content-up\">
      <img src='/modules/custom/university/images/body/gostinaya_001.jpg' alt=\"\" class=\"cent-up-image-param\">
    </div>
    <div class=\"center-content-center\">
      <img src='/modules/custom/university/images/body/room_002.jpg' alt=\"\" class=\"cent-cent-image-param-second\">
    </div>
    <div class=\"center-content-down\">
      <img src='/modules/custom/university/images/body/room_003.jpg' alt=\"\" class=\"cent-down-image-param-second\">
    </div>
    <div class=\"center-content-up-second\">
      <img src='/modules/custom/university/images/body/room_004.jpg' alt=\"\" class=\"cent-up-image-param-second\">
    </div>
    <div class=\"center-content-center-second\">
      <img src='/modules/custom/university/images/body/room_005.jpg' alt=\"\" class=\"cent-cont-image-param-second\">
    </div>
    <div class=\"center-content-down-second\">
      <img src='/modules/custom/university/images/body/room_006.jpg' alt=\"\" class=\"cent-down-image-param-second\">
    </div>
  </div>
  <div class=\"right-block\">
    <div class=\"right-content-up\">
      <ul class=\"right-content-up-list-up\">
        <li>about me</li>
      </ul>
    </div>
    <div class=\"right-content-center\">
      <ul class=\"right-content-center-list-up\">
        <li>about me</li>
      </ul>
    </div>
    <div class=\"right-content-down\">
      <ul class=\"right-content-down-list-up\">
        <li>about me</li>
      </ul>
    </div>
  </div>

</div>
<div class=\"main-content-body-middle\">
  <div class=\"left-main-content\">
    <p class=\"logo-left-main-content\">about</p>
    <ul class=\"ul-navigation-left-main-content\">
      <li class=\"li-nav-left-main-content\"><a>about</a></li>
      <li class=\"li-nav-left-main-content\"><a>/</a></li>
      <li class=\"li-nav-left-main-content\"><a>work</a></li>
      <li class=\"li-nav-left-main-content\"><a>/</a></li>
      <li class=\"li-nav-left-main-content\"><a>contact</a></li>
      <li class=\"li-nav-left-main-content\"><a>/</a></li>
      <li class=\"li-nav-left-main-content\"><a>shop</a></li>
    </ul>
  </div>
  <div class=\"main-content-body-after-middle\"></div>
  <div class=\"right-main-content\">
    <div class=\"right-title-content\">
      <p class=\"description-title-content\">I am a UX Designer and Art Director from Austria living in Berlin.</p>
      <p class=\"description-body-title-content\">A designer is a person who plans the form or structure
        of something before it is made, by preparing drawings or plans. In practice, anyone who creates
        tangible or intangible objects, products, processes, laws, games, graphics, services, or experiences
        can be referred to as a designer. In education, the methods of teaching or the program and theories
        followed vary according to schools and field of study. In industry, a design team for large projects
        is usually composed of a number of different types of designers and specialists. The relationships
        between team members will vary according to the proposed product, the processes of production or
        the research followed during the idea development, but normally they give an opportunity to everyone
        in the team to take a part in the creation process.
      </p>
    </div>
    <div class=\"right-head-content\">
      <p class=\"description-head-content\">Art Direction</p>
      <p class=\"description-body-head-content\">
        Starting with basic websites several years ago I found myself working as an Art Director for complex
        projects with a holistic approach soon. Visually appealing designs, subtle details and brand guidelines
        combined to innovative interfaces across various touch points became my daily companion.
      </p>
      <p class=\"description-body-part-two-head-content\">
        Out of this love for aesthetic design my passion for functionality and structure evolved. Jumping
        right into Photoshop didn’t feel accurate anymore and skipping the steps of building a framework based
        on functionality and usability became inevitable.
      </p>
    </div>
    <div class=\"right-body-content\">
      <p class=\"description-body-content\">Artworks and illustrations were my gateway to the creative industry which
        led
        to the foundation of my own studio and to first steps in the digital world.</p>
    </div>
    <div class=\"right-footer-content\">
      <p class=\"description-footer-content\">UX Design</p>
      <p class=\"description-over-navigation\">This rough framework outlines my process of developing digital
        experiences:</p>
      <ol class=\"description-after-over\">
        <li class=\"bold\">RESEARCH and gather the present state</li>
        <li class=\"bold\">STRUCTURE setup and content of the project</li>
        <li class=\"bold\">CONCEPT AND STRATEGY</li>
        <li class=\"bold\">CREATE, evaluate and iterate deliverables like</li>
      </ol>
      <ul class=\"navigate-description-footer-content\">
        <li>Personas</li>
        <li>Site maps</li>
        <li>Use cases and scenarios</li>
        <li>User flow</li>
        <li>Sketches and Infographics</li>
        <li>Wireframes</li>
      </ul>
      <div class=\"block-for-image-and-text-opozite\">
        <img src='/modules/custom/university/images/body/palm.jpg' alt=\"human-palm\" class=\"image-human-palm\">
        <p class=\"text-opozite-image\">To achieve the goal of making the web a better place I trust in teamwork, latest
          technology and intuition.</p>
      </div>
    </div>
  </div>
  <div class=\"down-block-content-left\">
    <div class=\"down-left-logo-navigate\">
      <div class=\"down-setting-logo-navigate-content\">
        <div class=\"words-logo-down-left-content\"><p class=\"logo-work\">work</p>
          <ul class=\"words-settings-down-left-content\">
            <li class=\"li-set-down-left\">Austria Tourism /</li>
            <li class=\"li-set-down-left\">Puma /</li>
            <li class=\"li-set-down-left\">Red Bull /</li>
            <li class=\"li-set-down-left\">myComfort</li>
          </ul>
        </div>
      </div>
      <div class=\"down-left-navigate-content-bottom\">
        <ul class=\"words-second-setting-down-left-content\">
          <li class=\"li-set-down-left\"><a>about /</a></li>
          <li class=\"li-set-down-left\"><a>work /</a></li>
          <li class=\"li-set-down-left\"><a>contact /</a></li>
          <li class=\"li-set-down-left\"><a>shop /</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class=\"down-middle-block-content-without\">
    <div class=\"down-m-b-c-w\"></div>
  </div>
  <div class=\"down-block-content-right\">
    <div class=\"down-first-content-image\"></div>
    <div class=\"down-second-content-image\"></div>
    <div class=\"down-third-content-image\"></div>
    <div class=\"down-fourth-content-image \"></div>
  </div>
</div>
</body>
</html>

{# <div> #}
{# {% if ageTyson < agePetro %} #}
{# <p>Tyson age is more then Petro</p> #}
{# <p> #}
{# ageTyson: {{ ageTyson }} #}
{# agePetro: {{ agePetro }} #}
{# </p> #}
{# {% endif %} #}
{# </div> #}

{# <h2>Account successfully created!</h2> #}

{# <p>Hello {{ name }}</p> #}

{# <p>Thank you for registering with us. Your account details are as follows: </p> #}
{# <p>You've already been logged in, so go on in and have some fun!</p> #}
", "modules/custom/university/templates/university-making-age.html.twig", "/app/web/modules/custom/university/templates/university-making-age.html.twig");
  }

  public function checkSecurity()
  {
    static $tags = array();
    static $filters = array("escape" => 1);
    static $functions = array("attach_library" => 1);

    try {
      $this->sandbox->checkSecurity(
        [],
        ['escape'],
        ['attach_library']
      );
    } catch (SecurityError $e) {
      $e->setSourceContext($this->source);

      if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
        $e->setTemplateLine($tags[$e->getTagName()]);
      } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
        $e->setTemplateLine($filters[$e->getFilterName()]);
      } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
        $e->setTemplateLine($functions[$e->getFunctionName()]);
      }

      throw $e;
    }

  }

  protected function doDisplay(array $context, array $blocks = [])
  {
    $macros = $this->macros;
    // line 1
    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->attachLibrary("generalblog/base-page"), "html", null, true);
    echo "
<!DOCTYPE html>
<html lang=\"en\">
<head class=\"main-class-head\">
  <title>The test page in base loc.</title>
</head>
<body>
<div class=\"main-content-body\">
  <div class=\"left-block\">
    <div class=\"left-content-up\">
      <ul class=\"left-content-up-list-up\">
        <li>about me</li>
      </ul>
    </div>
    <div class=\"left-content-center\">
      <ul class=\"left-content-center-list-up\">
        <li>scroll down</li>
      </ul>
    </div>
    <div class=\"left-content-down\">
      <ul class=\"left-content-down-list-up\">
        <li>ideas</li>
      </ul>
    </div>
  </div>
  <div class=\"center-block\">
    <div class=\"center-content-up\">
      <img src='/modules/custom/university/images/body/gostinaya_001.jpg' alt=\"\" class=\"cent-up-image-param\">
    </div>
    <div class=\"center-content-center\">
      <img src='/modules/custom/university/images/body/room_002.jpg' alt=\"\" class=\"cent-cent-image-param-second\">
    </div>
    <div class=\"center-content-down\">
      <img src='/modules/custom/university/images/body/room_003.jpg' alt=\"\" class=\"cent-down-image-param-second\">
    </div>
    <div class=\"center-content-up-second\">
      <img src='/modules/custom/university/images/body/room_004.jpg' alt=\"\" class=\"cent-up-image-param-second\">
    </div>
    <div class=\"center-content-center-second\">
      <img src='/modules/custom/university/images/body/room_005.jpg' alt=\"\" class=\"cent-cont-image-param-second\">
    </div>
    <div class=\"center-content-down-second\">
      <img src='/modules/custom/university/images/body/room_006.jpg' alt=\"\" class=\"cent-down-image-param-second\">
    </div>
  </div>
  <div class=\"right-block\">
    <div class=\"right-content-up\">
      <ul class=\"right-content-up-list-up\">
        <li>about me</li>
      </ul>
    </div>
    <div class=\"right-content-center\">
      <ul class=\"right-content-center-list-up\">
        <li>about me</li>
      </ul>
    </div>
    <div class=\"right-content-down\">
      <ul class=\"right-content-down-list-up\">
        <li>about me</li>
      </ul>
    </div>
  </div>

</div>
<div class=\"main-content-body-middle\">
  <div class=\"left-main-content\">
    <p class=\"logo-left-main-content\">about</p>
    <ul class=\"ul-navigation-left-main-content\">
      <li class=\"li-nav-left-main-content\"><a>about</a></li>
      <li class=\"li-nav-left-main-content\"><a>/</a></li>
      <li class=\"li-nav-left-main-content\"><a>work</a></li>
      <li class=\"li-nav-left-main-content\"><a>/</a></li>
      <li class=\"li-nav-left-main-content\"><a>contact</a></li>
      <li class=\"li-nav-left-main-content\"><a>/</a></li>
      <li class=\"li-nav-left-main-content\"><a>shop</a></li>
    </ul>
  </div>
  <div class=\"main-content-body-after-middle\"></div>
  <div class=\"right-main-content\">
    <div class=\"right-title-content\">
      <p class=\"description-title-content\">I am a UX Designer and Art Director from Austria living in Berlin.</p>
      <p class=\"description-body-title-content\">A designer is a person who plans the form or structure
        of something before it is made, by preparing drawings or plans. In practice, anyone who creates
        tangible or intangible objects, products, processes, laws, games, graphics, services, or experiences
        can be referred to as a designer. In education, the methods of teaching or the program and theories
        followed vary according to schools and field of study. In industry, a design team for large projects
        is usually composed of a number of different types of designers and specialists. The relationships
        between team members will vary according to the proposed product, the processes of production or
        the research followed during the idea development, but normally they give an opportunity to everyone
        in the team to take a part in the creation process.
      </p>
    </div>
    <div class=\"right-head-content\">
      <p class=\"description-head-content\">Art Direction</p>
      <p class=\"description-body-head-content\">
        Starting with basic websites several years ago I found myself working as an Art Director for complex
        projects with a holistic approach soon. Visually appealing designs, subtle details and brand guidelines
        combined to innovative interfaces across various touch points became my daily companion.
      </p>
      <p class=\"description-body-part-two-head-content\">
        Out of this love for aesthetic design my passion for functionality and structure evolved. Jumping
        right into Photoshop didn’t feel accurate anymore and skipping the steps of building a framework based
        on functionality and usability became inevitable.
      </p>
    </div>
    <div class=\"right-body-content\">
      <p class=\"description-body-content\">Artworks and illustrations were my gateway to the creative industry which
        led
        to the foundation of my own studio and to first steps in the digital world.</p>
    </div>
    <div class=\"right-footer-content\">
      <p class=\"description-footer-content\">UX Design</p>
      <p class=\"description-over-navigation\">This rough framework outlines my process of developing digital
        experiences:</p>
      <ol class=\"description-after-over\">
        <li class=\"bold\">RESEARCH and gather the present state</li>
        <li class=\"bold\">STRUCTURE setup and content of the project</li>
        <li class=\"bold\">CONCEPT AND STRATEGY</li>
        <li class=\"bold\">CREATE, evaluate and iterate deliverables like</li>
      </ol>
      <ul class=\"navigate-description-footer-content\">
        <li>Personas</li>
        <li>Site maps</li>
        <li>Use cases and scenarios</li>
        <li>User flow</li>
        <li>Sketches and Infographics</li>
        <li>Wireframes</li>
      </ul>
      <div class=\"block-for-image-and-text-opozite\">
        <img src='/modules/custom/university/images/body/palm.jpg' alt=\"human-palm\" class=\"image-human-palm\">
        <p class=\"text-opozite-image\">To achieve the goal of making the web a better place I trust in teamwork, latest
          technology and intuition.</p>
      </div>
    </div>
  </div>
  <div class=\"down-block-content-left\">
    <div class=\"down-left-logo-navigate\">
      <div class=\"down-setting-logo-navigate-content\">
        <div class=\"words-logo-down-left-content\"><p class=\"logo-work\">work</p>
          <ul class=\"words-settings-down-left-content\">
            <li class=\"li-set-down-left\">Austria Tourism /</li>
            <li class=\"li-set-down-left\">Puma /</li>
            <li class=\"li-set-down-left\">Red Bull /</li>
            <li class=\"li-set-down-left\">myComfort</li>
          </ul>
        </div>
      </div>
      <div class=\"down-left-navigate-content-bottom\">
        <ul class=\"words-second-setting-down-left-content\">
          <li class=\"li-set-down-left\"><a>about /</a></li>
          <li class=\"li-set-down-left\"><a>work /</a></li>
          <li class=\"li-set-down-left\"><a>contact /</a></li>
          <li class=\"li-set-down-left\"><a>shop /</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class=\"down-middle-block-content-without\">
    <div class=\"down-m-b-c-w\"></div>
  </div>
  <div class=\"down-block-content-right\">
    <div class=\"down-first-content-image\"></div>
    <div class=\"down-second-content-image\"></div>
    <div class=\"down-third-content-image\"></div>
    <div class=\"down-fourth-content-image \"></div>
  </div>
</div>
</body>
</html>

";
    // line 180
    echo "
";
    // line 182
    echo "
";
    // line 184
    echo "
";
  }
}
