<?php
/**
 * @file
 * Main file for hooks and custom functions.
 */

namespace Drupal\university\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "hello_block_two",
 *   admin_label = @Translation("Hello block Two"),
 *   category = @Translation("Hello World"),
 * )
 */
class HelloBlockTwo extends BlockBase
{

  public function build()
  {
    $config = Drupal::config('system.site');

    return [
      // '#markup' => $this->t('Hello block'.$message),
      '#markup' => $this->t(''),
    ];
  }

}
